import dateUtil from "./dateUtil";

let commentUtil = {

  isEmpty (obj) {
    if (obj === undefined) {
      return true;
    }
    if(obj === null){
      return true;
    }
    if(obj === ''){
      return true;
    }
    return false;
  },
  getContactNo() {//生成协议编号
    let contactNo = "";

    contactNo += dateUtil.format(new Date(),"YYYYMMDDHHmmssSSS");//生成前17位
    let randomStr = "";
    for (let i = 0; i < 9; i++) {
      let nu = Math.floor(Math.random() * 9);
      randomStr += nu;
    }


    return contactNo.concat(randomStr);
  },
  showDialog(thisVue,message, func,cancalfunc) {
    let dialog = thisVue.$dialog.confirm({
      message: message,
    });
    if (func !== undefined) {
      dialog.then(func);
    }
    if (cancalfunc !== undefined) {
      dialog.catch(cancalfunc);
    }

  },
  showAlertDialog(thisVue,message, func){
    let dialog = thisVue.$dialog.alert({
      message: message,
    });
    if(func !== undefined){
      dialog.then(func);
    }
  },
  formatStartDate(data){
    return data + ' 00:00:00'
  },
  formatEndDate(data){
    return data + ' 23:59:59'
  }
};
export default commentUtil
