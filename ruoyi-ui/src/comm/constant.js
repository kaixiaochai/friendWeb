export const SUCCESS = 200;
export const GENDER = [
  { text: '男', value: 1 },
  { text: '女', value: 0 },
];
export const ACTION_TYPE = [
  { text: '旅行', value: 1 },
  { text: '聚会', value: 2 },
  { text: '约饭', value: 3 },
  { text: '看电影', value: 4 },
  { text: '蹦迪', value: 5 },
];
export const ORDER_STATUS = [
  { text: '失败', value: 'FAILED' },
  { text: '成功', value: 'SUCCESS' },
  { text: '处理中', value: 'IN_PROCESS' },
];
export const ORDER_TYPE = [
  { text: '升级缴费', value: 'UPGRADE' },
];
export const PAY_TYPE = [
  { text: '微信支付', value: 'WECHAT' },
  { text: '支付宝支付', value: 'ALIPAY' },
];
export const SWITCH_TYPE = [
  { text: '否', value: 0 },
  { text: '是', value: 1 },
];
