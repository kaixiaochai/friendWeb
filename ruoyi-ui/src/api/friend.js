import request from '@/utils/request'

/**
 * 分页查询用户
 * @param data
 */
export function userPage(data) {
  return get('/user/page',data);
}
/**
 * 查询用户
 * @param data
 */
export function getUser(data) {
  return get('/user/getUser',data);
}
/**
 * 活动分页
 * @param data
 */
export function actionPage(data) {
  return get('/activity/page',data);
}
/**
 * 订单分页
 * @param data
 */
export function orderPage(data) {
  return get('/appPaymentOrder/page',data);
}
/**
 * 喜欢分页
 * @param data
 */
export function likePage(data) {
  return get('/appUserLike/page',data);
}
/**
 * 喜欢
 * @param data
 */
export function like(data) {
  return post('/appUserLike/like',data);
}
/**
 * 黑名单分页
 * @param data
 */
export function blackPage(data) {
  return get('/appUserBlackList/page',data);
}
/**
 * 黑名单
 * @param data
 */
export function black(data) {
  return post('/appUserBlackList/black',data);
}
/**
 * 添加邀请码
 * @param data
 */
export function inviteCodeAdd(data) {
  return post('/appInvite/add',data);
}
/**
 * 分页查询邀请码
 * @param data
 */
export function inviteCodePage(data) {
  return get('/appInvite/page',data);
}
/**
 * 分页查询用户账户
 * @param data
 */
export function accountPage(data) {
  return get('/appUserAccount/page',data);
}
/**
 * 查询缴费策略
 * @param data
 */
export function policyList(data) {
  return get('/appUserPolicy/list',data);
}
/**
 * 初始化缴费策略
 * @param data
 */
export function policyInit(data) {
  return post('/appUserPolicy/initPolicy',data);
}
/**
 * 添加缴费策略
 * @param data
 */
export function policyAdd(data) {
  return post('/appUserPolicy/addPolicy',data);
}
/**
 * 更新缴费策略
 * @param data
 */
export function policyUpdate(data) {
  return post('/appUserPolicy/updatePolicy',data);
}

function post(url, data) {
  return request({
    url: url,
    method: 'post',
    data: data
  })
}

function get(url, data) {

  return request({
    url: url,
    method: 'get',
    params: data
  })
}
