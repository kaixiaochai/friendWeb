package com.village.friend.exception;

import lombok.Data;

@Data
public class AppRuntimeException extends RuntimeException {

    private String errorCode;

    public AppRuntimeException(String errorCode, String message) {
        super(message);
        this.errorCode = errorCode;
    }

    public AppRuntimeException(String message) {
        super(message);
    }

}
