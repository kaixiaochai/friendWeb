package com.village.friend;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@MapperScan("com.village.friend.mapper")
@MapperScan("com.ruoyi.*.mapper")
@SpringBootApplication(scanBasePackages = {"com.ruoyi","com.village.friend"}, exclude = { DataSourceAutoConfiguration.class })
public class FriendApplication {

    public static void main(String[] args) {
        SpringApplication.run(FriendApplication.class, args);
    }

}
