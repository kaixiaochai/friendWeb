package com.village.friend.dto.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@ApiModel(description = "活动报名和取消")
public class ActivityJoinDto{
    @ApiModelProperty(notes = "活动ID")
    Integer id;
    @ApiModelProperty(notes = "是否加入 0 取消 1 加入")
    Integer isJoin;
    @ApiModelProperty(notes = "用户手机号")
    String username;
}
