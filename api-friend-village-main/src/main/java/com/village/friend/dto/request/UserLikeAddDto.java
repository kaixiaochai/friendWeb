package com.village.friend.dto.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@ApiModel(description = "获取用户信息")
public class UserLikeAddDto {

    @ApiModelProperty(notes = "添加喜欢的用户")
    String username;

    @ApiModelProperty(notes = "被喜欢的用户")
    String likeUser;

    @ApiModelProperty(notes = "是否喜欢 0 不喜欢, 1 喜欢")
    Integer like;
}
