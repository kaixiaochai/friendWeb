package com.village.friend.dto.request;

import com.village.friend.entity.ColValueBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class ProfitPolicyAddDto {

    @ApiModelProperty(value = "vip等级")
    private Integer grade;

    @ApiModelProperty(value = "等级的分润配置列表")
    private List<ColValueBean> list;



}
