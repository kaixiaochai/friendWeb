package com.village.friend.dto.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class AppInviteAddDto {

    @ApiModelProperty(value = "邀请码数量")
    private Integer count;


}
