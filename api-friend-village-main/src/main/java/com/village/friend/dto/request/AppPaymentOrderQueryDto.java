package com.village.friend.dto.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class AppPaymentOrderQueryDto extends BasePageDto {

    @ApiModelProperty(value = "交易用户")
    private String username;

    @ApiModelProperty(value = "订单编号")
    private String orderNo;

    @ApiModelProperty(value = "订单状态")
    private String payStatus;

    @ApiModelProperty(value = "订单类型")
    private String payTypeNo;

    @ApiModelProperty(value = "支付方式: WECHAT,ALIPAY")
    private String payWayName;


}
