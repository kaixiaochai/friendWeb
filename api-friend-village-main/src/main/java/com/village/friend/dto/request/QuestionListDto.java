package com.village.friend.dto.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@ApiModel(description = "用户问题请求")
public class QuestionListDto extends BasePageDto {
    @ApiModelProperty(value = "id")
    Integer id;
    @ApiModelProperty(value = "用户名")
    String username;
}
