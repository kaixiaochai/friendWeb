package com.village.friend.dto.request;


import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@ApiModel(description = "升级缴费")
public class UpgradePayDto {

    @ApiModelProperty(value = "会员时间 1,3,12(月)")
    private Integer time;

    @ApiModelProperty(value = "用户手机号")
    private String username;

    @ApiModelProperty(value = "支付类型 wechat,aliAppPay,aliH5Pay")
    private String payType;

}
