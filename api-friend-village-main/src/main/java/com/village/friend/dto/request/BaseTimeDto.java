package com.village.friend.dto.request;



/**
 * @author: santana
 * @date : 2021年9月14日18:01:13
 * @time : 15:40
 * @Desc : 时间查询基类
 * @since : 1.0.0
 */
public interface BaseTimeDto {

    String getStartTime();

    void setStartTime(String startTime);

    String getEndTime();

    void setEndTime(String endTime);
}
