package com.village.friend.dto.request;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.village.friend.constant.PolicyType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@ApiModel(description = "缴费策略更新")
public class PolicyUpdateDto {

    @ApiModelProperty(value = "政策类型: UPGRADE")
    private String policyType;

    @ApiModelProperty(value = "vip时间 单位月")
    private String policyGrade;

    @ApiModelProperty(value = "开始级别 0 ")
    private String sourceGrade = "0";

    @ApiModelProperty(value = "政策内容: 缴费金额(元)")
    private String policyContent;

}
