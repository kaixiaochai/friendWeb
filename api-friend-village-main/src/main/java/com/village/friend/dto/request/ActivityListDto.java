package com.village.friend.dto.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@ApiModel(description = "活动列表请求")
public class ActivityListDto extends BasePageDto {
    @ApiModelProperty(value = "请求用户")
    private String username;
    @ApiModelProperty(value = "活动类型")
    Integer type;
    @ApiModelProperty(value = "市")
    String city;
}
