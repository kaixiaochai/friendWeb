package com.village.friend.dto.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.util.List;

@Data
public class UpdateUserDto {
    @ApiModelProperty(notes = "账户")
    String username;
    @ApiModelProperty(notes = "昵称")
    String nickname;
    @ApiModelProperty(notes = "头像")
    String avatarurl;
    @ApiModelProperty(notes = "生日")
    String birth;
    @ApiModelProperty(notes = "年龄")
    Integer age;
    @ApiModelProperty(notes = "身高")
    Integer height;
    @ApiModelProperty(notes = "体重")
    Integer weight;
    @ApiModelProperty(notes = "相册")
    List<String> photos;
    @ApiModelProperty(notes = "是1否0隐藏地址")
    Integer isHide;
    @ApiModelProperty(notes = "是1否0注销")
    Integer isLogout;
    @ApiModelProperty(notes = "标签")
    List<String> flags;
    @ApiModelProperty(notes = "常驻城市")
    String city;
    @ApiModelProperty(notes = "自我介绍")
    String selfDesc;
    @ApiModelProperty(notes = "职业")
    String profession;
    @ApiModelProperty(notes = "年薪")
    String salaryYear;
    @ApiModelProperty(notes = "微信")
    String weChatAccount;
    @ApiModelProperty(notes = "上级邀请码")
    String inviteCode;

}
