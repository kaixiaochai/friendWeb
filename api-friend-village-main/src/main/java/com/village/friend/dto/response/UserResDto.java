package com.village.friend.dto.response;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.util.List;

@Data
public class UserResDto {
    @ApiModelProperty(notes = "账户")
    String username;
    @ApiModelProperty(notes = "用户头像")
    String avatarurl;
    @ApiModelProperty(notes = "昵称")
    String nickname;
    @ApiModelProperty(notes = "年龄")
    Integer age;
    @ApiModelProperty(notes = "身高")
    Integer height;
    @ApiModelProperty(notes = "体重")
    Integer weight;
    @ApiModelProperty(notes = "我的相册")
    List<String> photos;
    @ApiModelProperty(notes = "是1否0隐藏地址")
    Integer isHide;
    @ApiModelProperty(notes = "vip等级：0不是vip 1:青铜 2：白银 3:黄金")
    Integer vipLevel;
    @ApiModelProperty(notes = "vip剩余天数(10000代表永久会员)")
    Integer vipLeft;
    @ApiModelProperty(notes = "常驻城市")
    String city;
    @ApiModelProperty(notes = "自我介绍")
    String selfDesc;
    @ApiModelProperty(notes = "职业")
    String profession;
    @ApiModelProperty(notes = "年薪")
    String salaryYear;
    @ApiModelProperty(notes = "微信")
    String weChatAccount;
    @ApiModelProperty(notes = "标签")
    List<String> flags;
    @ApiModelProperty(notes = "是1否0在线")
    Integer online;
    @ApiModelProperty(notes = "距离")
    Double distance;
    @ApiModelProperty(notes = "token")
    String token;
    @ApiModelProperty(notes = "邀请码")
    String inviteCode;

}
