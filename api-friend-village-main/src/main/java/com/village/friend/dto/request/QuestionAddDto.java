package com.village.friend.dto.request;


import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@ApiModel(description = "问题")
public class QuestionAddDto {

    @ApiModelProperty(value = "用户名")
    private String username;

    @ApiModelProperty(value = "问题描述")
    private String question;

    @ApiModelProperty(value = "联系方式")
    private String contact;

    @ApiModelProperty(value = "配图路径,最多3张")
    private List<String> imageList;

}
