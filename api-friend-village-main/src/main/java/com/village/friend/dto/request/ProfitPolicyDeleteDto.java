package com.village.friend.dto.request;

import com.village.friend.entity.ColValueBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class ProfitPolicyDeleteDto {

    @ApiModelProperty(value = "策略id")
    private Integer id;



}
