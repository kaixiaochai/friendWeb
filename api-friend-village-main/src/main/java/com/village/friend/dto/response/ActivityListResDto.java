package com.village.friend.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@ApiModel(description = "返回活动列表信息")
public class ActivityListResDto {

    @ApiModelProperty(value = "id")
    private Integer id;

    @ApiModelProperty(value = "活动类型")
    private Integer type;

    @ApiModelProperty(value = "发起人手机号")
    private String username;

    @ApiModelProperty(value = "活动地址")
    private String city;


    @ApiModelProperty(value = "微信号")
    private String wechat;

    @ApiModelProperty(value = "期待对象")
    private Integer destType;

    @ApiModelProperty(value = "活动时间类型")
    private Integer timeType;

    @ApiModelProperty(value = "活动时间")
    private LocalDateTime time;

    @ApiModelProperty(value = "补充说明")
    private String instr;

    @ApiModelProperty(value = "配图路径,最多3张")
    private List<String> images;

    @ApiModelProperty(value = "报名用户,多个用逗号隔开")
    private List<String> joinUids;

    @ApiModelProperty(notes = "是否加入 0 取消 1 加入")
    Integer isJoin;

    @ApiModelProperty(value = "距离")
    Double distance;
    @ApiModelProperty(notes = "用户头像")
    String avatarurl;
    @ApiModelProperty(notes = "昵称")
    String nickname;
    @ApiModelProperty(notes = "性别:1男 2女")
    Integer gender;
    @ApiModelProperty(notes = "vip等级：0不是vip 1:青铜 2：白银 3:黄金")
    Integer vipLevel;
    @ApiModelProperty(value = "发布时间时间")
    private LocalDateTime createTime;


}
