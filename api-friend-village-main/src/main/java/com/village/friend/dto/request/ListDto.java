package com.village.friend.dto.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@ApiModel(description = "首页列表请求")
public class ListDto extends BasePageDto {
    @ApiModelProperty(value = "账户")
    String username;
    @ApiModelProperty(value = "请求方式：1：在线 2：附近 3：新注册")
    Integer mode;
    @ApiModelProperty(value = "开始年龄")
    Integer ageStart;
    @ApiModelProperty(value = "结束年龄")
    Integer ageEnd;
    @ApiModelProperty(value = "年收入")
    Integer salaryYear;
    @ApiModelProperty(value = "城市")
    String city;
    @ApiModelProperty(value = "经度")
    Double latitude;
    @ApiModelProperty(value = "纬度")
    Double longitude;
}
