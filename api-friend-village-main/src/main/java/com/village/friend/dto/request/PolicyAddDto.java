package com.village.friend.dto.request;


import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@ApiModel(description = "添加缴费策略")
public class PolicyAddDto {

    @ApiModelProperty(value = "生效时间")
    private Integer time;

    @ApiModelProperty(value = "缴费金额")
    private Integer amount;

}
