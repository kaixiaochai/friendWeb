package com.village.friend.dto.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@ApiModel(description = "获取用户信息")
public class UserBlackListAddDto {

    @ApiModelProperty(notes = "添加喜欢的用户")
    String username;

    @ApiModelProperty(notes = "被操作的用户")
    String blackUser;

    @ApiModelProperty(notes = "是否喜欢 0 删除黑名单, 1 添加黑名单")
    Integer black;
}
