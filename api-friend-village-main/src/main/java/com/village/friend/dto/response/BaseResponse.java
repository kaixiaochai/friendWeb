package com.village.friend.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.village.friend.constant.MsgCodeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Map;

/**
 * 此类的描述是：
 *
 * @author ant_li@qq.com
 * @createTime 2019-07-31
 **/
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@ApiModel(description = "数据返回对象")
public class BaseResponse<T> {

    /**
     * 返回码
     */
    @ApiModelProperty(notes = "返回码")
    private int code;
    /**
     * 返回信息
     */
    @ApiModelProperty(notes = "返回信息")
    private String msg;
    /**
     * 返回数据
     */
    @ApiModelProperty(notes = "返回数据")
    private T data;


    public BaseResponse() {
    }

    public BaseResponse(MsgCodeEnum msgCodeEnum) {
        this.code = msgCodeEnum.getCode();
        this.msg = msgCodeEnum.getMsg();
    }

    public BaseResponse(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public BaseResponse(MsgCodeEnum msgCodeEnum, T data) {
        this.code = msgCodeEnum.getCode();
        this.msg = msgCodeEnum.getMsg();
        this.data = data;
    }


    public static BaseResponse success(Map<String,Object> resultMap){
        BaseResponse respDto = result(MsgCodeEnum.SUCCESS);
        respDto.setData(resultMap);
        return respDto;
    }

    public static <T> BaseResponse success(T data){
        BaseResponse respDto = result(MsgCodeEnum.SUCCESS);
        respDto.setData(data);
        return respDto;
    }

    public static BaseResponse success(){
        return result(MsgCodeEnum.SUCCESS);
    }

    public static BaseResponse paramsError(){
        return result(MsgCodeEnum.PARAMS_ERROR);
    }
    public static BaseResponse paramsError(String msg){
        BaseResponse response = result(MsgCodeEnum.PARAMS_ERROR);
        response.setMsg(msg);
        return response;
    }

    public static BaseResponse error(String msg){
        BaseResponse result = result(MsgCodeEnum.OTHER_ERROR);
        result.setMsg(msg);
        return result;
    }

    public static BaseResponse error(Integer code, String msg){
        BaseResponse result = result(MsgCodeEnum.OTHER_ERROR);
        result.setMsg(msg);
        result.code = code;
        return result;
    }



    public static BaseResponse result(MsgCodeEnum codeEnum){
        BaseResponse respDto = new BaseResponse();
        respDto.setCode(codeEnum.getCode());
        respDto.setMsg(codeEnum.getMsg());
        return respDto;
    }


}
