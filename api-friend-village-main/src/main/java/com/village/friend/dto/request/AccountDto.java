package com.village.friend.dto.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class AccountDto extends BasePageDto implements BaseTimeDto{
    @ApiModelProperty(value = "id")
    String id;

    @ApiModelProperty(value = "邀请码")
    String userId;

    @ApiModelProperty(value = "开始时间 yyyy-MM-dd HH:mm:ss")
    String startTime;

    @ApiModelProperty(value = "结束时间 yyyy-MM-dd HH:mm:ss")
    String endTime;


}
