package com.village.friend.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@ApiModel(description = "关键词：对活动的要求，关于我，对爱的看法（用,分隔）")
public class KeyWordsResDto {

    @ApiModelProperty(notes = "对活动的要求")
    List<String> aboutPlay;

    @ApiModelProperty(notes = "关于我")
    List<String> aboutMe;

    @ApiModelProperty(notes = "对爱的看法")
    List<String> aboutLove;

}
