package com.village.friend.dto.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class AppUserProfitQueryDto extends BasePageDto {

    @ApiModelProperty(value = "收益人username")
    private String user;

    @ApiModelProperty(value = "交易人username")
    private String transUser;

    @ApiModelProperty(value = "订单编号")
    private String orderNo;

    @ApiModelProperty(value = "分润类型")
    private String profitType;


}
