package com.village.friend.dto.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ProfitPolicyQueryDto {

    @ApiModelProperty(value = "策略id")
    private Integer id;

    @ApiModelProperty(value = "策略分类")
    private String colGroup;

    @ApiModelProperty(value = "策略键")
    private String colKey;



}
