package com.village.friend.dto.request;


import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@ApiModel(description = "缴费策略配置")
public class PolicyInitDto {

    @ApiModelProperty(value = "升级1月所需金额")
    private Integer month1;

    @ApiModelProperty(value = "升级3个月所需金额")
    private Integer month3;

    @ApiModelProperty(value = "升级6个月所需金额")
    private Integer month6;

    @ApiModelProperty(value = "升级12个月所需金额")
    private Integer month12;

}
