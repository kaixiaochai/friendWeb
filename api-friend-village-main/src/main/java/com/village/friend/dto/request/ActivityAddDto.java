package com.village.friend.dto.request;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@ApiModel(description = "活动报名和取消")
public class ActivityAddDto {

    @ApiModelProperty(value = "活动类型")
    private Integer type;

    @ApiModelProperty(value = "发起人手机号")
    private String username;

    @ApiModelProperty(value = "活动地址")
    private String city;

    @ApiModelProperty(value = "期待对象")
    private Integer destType;

    @ApiModelProperty(value = "活动时间类型")
    private Integer timeType;

    @ApiModelProperty(value = "活动时间")
    private LocalDateTime time;

    @ApiModelProperty(value = "补充说明")
    private String instr;

    @ApiModelProperty(value = "微信号")
    private String wechat;

    @ApiModelProperty(value = "配图路径,最多3张")
    private List<String> images;

}
