package com.village.friend.dto.request;


import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author: santana
 * @date : 2018/11/9
 * @time : 15:40
 * @Desc :
 * @since : 1.0.0
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@ApiModel(description = "分页查询基类")
public class BasePageDto implements Serializable {

    @ApiModelProperty(notes = "每页显示数量",value = "每页显示数量")
    private Integer limit = 15;

    @ApiModelProperty(notes = "当前页数",value = "每页显示数量")
    private Integer page = 1;

    public Integer getLimit() {
        if (null == limit || limit.intValue() < 1) {
            return 15;
        }
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getPage() {
        if (null == page || page.intValue() < 0) {
            return 1;
        }
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }


}
