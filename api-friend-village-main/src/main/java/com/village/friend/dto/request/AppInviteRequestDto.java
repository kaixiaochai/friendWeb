package com.village.friend.dto.request;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class AppInviteRequestDto extends BasePageDto{
    @ApiModelProperty(value = "id")
    private String id;

    @ApiModelProperty(value = "邀请码")
    private String inviteCode;

    @ApiModelProperty(value = "0未分配，1已分配")
    private Integer isused;

    @ApiModelProperty(value = "开始时间 yyyy-MM-dd HH:mm:ss")
    String startTime;

    @ApiModelProperty(value = "结束时间 yyyy-MM-dd HH:mm:ss")
    String endTime;


}
