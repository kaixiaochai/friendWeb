package com.village.friend.dto.request;


import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@ApiModel(description = "缴费策略查询")
public class PolicyListDto {

    @ApiModelProperty(value = "政策类型 UPGRADE")
    private String policyType;

    @ApiModelProperty(value = "有效时间")
    private Integer time;

}
