package com.village.friend.dto.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@ApiModel(description = "登录")
public class HeartDto {
    @ApiModelProperty(notes = "账户")
    String username;
    @ApiModelProperty(notes = "ip")
    String ip;
    @ApiModelProperty(notes = "经度")
    Double latitude;
    @ApiModelProperty(notes = "纬度")
    Double longitude;
}

