package com.village.friend.dto.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@ApiModel(description = "分页查询用户")
public class UserPageDto extends BasePageDto implements BaseTimeDto{

    @ApiModelProperty(value = "用户编号")
    String username;

    @ApiModelProperty(value = "昵称模糊")
    String nickname;

    @ApiModelProperty(value = "电话（模糊）")
    String phone;

    @ApiModelProperty(value = "开始时间 yyyy-MM-dd HH:mm:ss")
    String startTime;

    @ApiModelProperty(value = "结束时间 yyyy-MM-dd HH:mm:ss")
    String endTime;
}
