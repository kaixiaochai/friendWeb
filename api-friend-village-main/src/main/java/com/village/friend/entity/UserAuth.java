package com.village.friend.entity;

import lombok.Data;

/**
 * 用户权限相关
 */
@Data
public class UserAuth {
    String username;
    String password;
    String authCode;
    String token;
    String createTime;
    String updateTime;
}
