package com.village.friend.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;

import java.time.LocalDateTime;
import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>
 *
 * </p>
 *
 * @author yl
 * @since 2021-08-13
 */
@Data
@ApiModel(value = "Activity", discriminator = "", subTypes = {Activity.class})
public class Activity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private Integer type;
    private String username;
    private String city;
    private Integer destType;
    private Integer timeType;
    private LocalDateTime time;
    private String instr;
    private String images;
    private String joinUids;
    private String wechat;
    private LocalDateTime createTime;
    private LocalDateTime updateTime;
    // 衍生字段，不是数据库字段
    @TableField(exist = false)
    Double distance;
    @TableField(exist = false)
    String nickname;
    @TableField(exist = false)
    String avatarurl;
    @TableField(exist = false)
    Integer gender;
    @TableField(exist = false)
    Integer vipLevel;



    @Override
    public String toString() {
        return "Activity{" +
                ", id=" + id +
                ", type=" + type +
                ", username=" + username +
                ", city=" + city +
                ", destType=" + destType +
                ", timeType=" + timeType +
                ", time=" + time +
                ", desc=" + instr +
                ", images=" + images +
                ", joinUids=" + joinUids +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                "}";
    }
}
