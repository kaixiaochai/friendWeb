package com.village.friend.entity;

import lombok.Data;

/**
 * 配置分润比例的bean
 */
@Data
public class ColValueBean {
    Integer level;
    Integer value;
}
