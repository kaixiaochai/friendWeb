package com.village.friend.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>
 * 用户收益明细表
 * </p>
 *
 * @author yl
 * @since 2021-08-23
 */
@Data
@ApiModel(value="AppUserProfit对象", description="用户收益明细表")
public class AppUserProfit implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "id")
    @TableId(value = "id", type = IdType.ASSIGN_UUID)
    private String id;

    @ApiModelProperty(value = "用户ID（收益人user_id）")
    private String userId;

    @ApiModelProperty(value = "用户名字（收益人名字）")
    private String fullName;

    @ApiModelProperty(value = "交易用户ID（触发人user_id）")
    private String transUserId;

    @ApiModelProperty(value = "交易用户名字")
    private String transFullName;

    @ApiModelProperty(value = "交易金额")
    private BigDecimal transAmount;

    @ApiModelProperty(value = "交易订单号")
    private String orderNo;

    @ApiModelProperty(value = "用户收益")
    private BigDecimal userProfit;

    @ApiModelProperty(value = "分润比例")
    private BigDecimal profitPercent;

    @ApiModelProperty(value = "分润类型")
    private String profitType;

    @ApiModelProperty(value = "备注")
    private String remark;

    private LocalDateTime createTime;

    @ApiModelProperty(value = "更新时间")
    private LocalDateTime updateTime;




    @Override
    public String toString() {
        return "AppUserProfit{" +
        "id=" + id +
        ", userId=" + userId +
        ", fullName=" + fullName +
        ", transUserId=" + transUserId +
        ", transFullName=" + transFullName +
        ", transAmount=" + transAmount +
        ", orderNo=" + orderNo +
        ", userProfit=" + userProfit +
        ", profitPercent=" + profitPercent +
        ", profitType=" + profitType +
        ", remark=" + remark +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        "}";
    }
}
