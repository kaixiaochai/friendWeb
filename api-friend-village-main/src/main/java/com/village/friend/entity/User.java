package com.village.friend.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * <p>
 * 用户表
 * </p>
 *
 * @author yl
 * @since 2021-08-20
 */
@Data
@ApiModel(value = "User对象", description = "用户表")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    // 根据已有字段算出来的数据,不是数据库原生数据
    @TableField(exist = false)
    Integer age;
    @TableField(exist = false)
    Integer vipLeft;
    @TableField(exist = false)
    Double distance;
    @TableField(exist = false)
    Integer online;
    @TableField(exist = false)
    String token;

    @ApiModelProperty(value = "用户ID")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "账号")
    private String username;

    @ApiModelProperty(value = "昵称")
    private String nickname;

    @ApiModelProperty(value = "电话")
    private String phone;

    @ApiModelProperty(value = "头像")
    private String avatarurl;

    @ApiModelProperty(value = "性别 0男 ,1 女")
    private Integer gender;

    @ApiModelProperty(value = "生日")
    private LocalDate birth;

    @ApiModelProperty(value = "身高")
    private Integer height;

    @ApiModelProperty(value = "体重")
    private Integer weight;

    @ApiModelProperty(value = "我的相册")
    private String photos;

    @ApiModelProperty(value = "是1否0隐藏地址")
    private Integer isHide;

    @ApiModelProperty(value = "是1否0冻结")
    private Integer isFreeze;

    @ApiModelProperty(value = "是1否0注销")
    private Integer isLogout;

    @ApiModelProperty(value = "上级用户名")
    private String parent;

    @ApiModelProperty(value = "邀请码")
    private String inviteCode;

    @ApiModelProperty(value = "vip等级：0不是vip 1:青铜 2：白银 3:黄金")
    private Integer vipLevel;

    @ApiModelProperty(value = "vip截止时间")
    private LocalDateTime vipEnd;

    @ApiModelProperty(value = "常驻城市")
    private String city;

    @ApiModelProperty(value = "自我介绍")
    private String selfDesc;

    @ApiModelProperty(value = "职业")
    private String profession;

    @ApiModelProperty(value = "年薪(万)")
    private String salaryYear;

    @ApiModelProperty(value = "微信")
    private String wechatAccount;

    @ApiModelProperty(value = "标签")
    private String flags;

    @ApiModelProperty(value = "建立时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "最后更新时间")
    private LocalDateTime updateTime;


    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username=" + username +
                ", nickname=" + nickname +
                ", avatarurl=" + avatarurl +
                ", gender=" + gender +
                ", birth=" + birth +
                ", height=" + height +
                ", weight=" + weight +
                ", photos=" + photos +
                ", isHide=" + isHide +
                ", isFreeze=" + isFreeze +
                ", isLogout=" + isLogout +
                ", inviteCode=" + inviteCode +
                ", vipLevel=" + vipLevel +
                ", vipEnd=" + vipEnd +
                ", city=" + city +
                ", selfDesc=" + selfDesc +
                ", profession=" + profession +
                ", salaryYear=" + salaryYear +
                ", wechatAccount=" + wechatAccount +
                ", flags=" + flags +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                "}";
    }
}
