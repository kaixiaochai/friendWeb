package com.village.friend.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 交易订单
 * </p>
 *
 * @author yl
 * @since 2021-08-23
 */
@ApiModel(value="AppPaymentOrder对象", description="交易订单")
public class AppPaymentOrder implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "ID主键")
    @TableId(value = "id", type = IdType.ASSIGN_UUID)
    private String id;

    @ApiModelProperty(value = "用户id")
    private String userId;

    @ApiModelProperty(value = "用户名称")
    private String fullName;

    @ApiModelProperty(value = "商品名称")
    private String productName;

    @ApiModelProperty(value = "订单金额")
    private BigDecimal orderAmount;

    @ApiModelProperty(value = "分润金额")
    private BigDecimal profitAmount;

    @ApiModelProperty(value = "支付状态 见支付状态枚举")
    private String payStatus;

    @ApiModelProperty(value = "失败原因")
    private String failedReason;

    @ApiModelProperty(value = "订单有效期")
    private Integer orderPeriod;

    @ApiModelProperty(value = "到期时间")
    private LocalDateTime expireTime;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "支付渠道名称")
    private String payWayName;

    @ApiModelProperty(value = "支付类型编码")
    private String payTypeNo;

    @ApiModelProperty(value = "支付类型名称")
    private String payTypeName;

    @ApiModelProperty(value = "是否分账  0：否 1：是 ")
    private String splitFlag;

    @ApiModelProperty(value = "是否对账  0：否 1：是 ")
    private String reconFlag;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "修改时间")
    private LocalDateTime updateTime;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public BigDecimal getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(BigDecimal orderAmount) {
        this.orderAmount = orderAmount;
    }

    public BigDecimal getProfitAmount() {
        return profitAmount;
    }

    public void setProfitAmount(BigDecimal profitAmount) {
        this.profitAmount = profitAmount;
    }

    public String getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(String payStatus) {
        this.payStatus = payStatus;
    }

    public String getFailedReason() {
        return failedReason;
    }

    public void setFailedReason(String failedReason) {
        this.failedReason = failedReason;
    }

    public Integer getOrderPeriod() {
        return orderPeriod;
    }

    public void setOrderPeriod(Integer orderPeriod) {
        this.orderPeriod = orderPeriod;
    }

    public LocalDateTime getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(LocalDateTime expireTime) {
        this.expireTime = expireTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getPayWayName() {
        return payWayName;
    }

    public void setPayWayName(String payWayName) {
        this.payWayName = payWayName;
    }

    public String getPayTypeNo() {
        return payTypeNo;
    }

    public void setPayTypeNo(String payTypeNo) {
        this.payTypeNo = payTypeNo;
    }

    public String getPayTypeName() {
        return payTypeName;
    }

    public void setPayTypeName(String payTypeName) {
        this.payTypeName = payTypeName;
    }

    public String getSplitFlag() {
        return splitFlag;
    }

    public void setSplitFlag(String splitFlag) {
        this.splitFlag = splitFlag;
    }

    public String getReconFlag() {
        return reconFlag;
    }

    public void setReconFlag(String reconFlag) {
        this.reconFlag = reconFlag;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "AppPaymentOrder{" +
        "id=" + id +
        ", userId=" + userId +
        ", fullName=" + fullName +
        ", productName=" + productName +
        ", orderAmount=" + orderAmount +
        ", profitAmount=" + profitAmount +
        ", payStatus=" + payStatus +
        ", failedReason=" + failedReason +
        ", orderPeriod=" + orderPeriod +
        ", expireTime=" + expireTime +
        ", remark=" + remark +
        ", payWayName=" + payWayName +
        ", payTypeNo=" + payTypeNo +
        ", payTypeName=" + payTypeName +
        ", splitFlag=" + splitFlag +
        ", reconFlag=" + reconFlag +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        "}";
    }
}
