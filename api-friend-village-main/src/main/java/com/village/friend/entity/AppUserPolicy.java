package com.village.friend.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 会员缴费政策表
 * </p>
 *
 * @author yl
 * @since 2021-08-23
 */
@ApiModel(value="AppUserPolicy对象", description="会员缴费政策表")
public class AppUserPolicy implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "政策类型 pay，customer，transamount")
    private String policyType;

    @ApiModelProperty(value = "类型描述 缴费，消费，交易金额，交易笔数，推荐")
    private String typeDesc;

    @ApiModelProperty(value = "政策内容")
    private String policyContent;

    @ApiModelProperty(value = "目标级别")
    private String policyGrade;

    @ApiModelProperty(value = "政策名称")
    private String policyName;

    @ApiModelProperty(value = "开始级别,升级条件")
    private String sourceGrade;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "更新时间")
    private LocalDateTime updateTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPolicyType() {
        return policyType;
    }

    public void setPolicyType(String policyType) {
        this.policyType = policyType;
    }

    public String getTypeDesc() {
        return typeDesc;
    }

    public void setTypeDesc(String typeDesc) {
        this.typeDesc = typeDesc;
    }

    public String getPolicyContent() {
        return policyContent;
    }

    public void setPolicyContent(String policyContent) {
        this.policyContent = policyContent;
    }

    public String getPolicyGrade() {
        return policyGrade;
    }

    public void setPolicyGrade(String policyGrade) {
        this.policyGrade = policyGrade;
    }

    public String getPolicyName() {
        return policyName;
    }

    public void setPolicyName(String policyName) {
        this.policyName = policyName;
    }

    public String getSourceGrade() {
        return sourceGrade;
    }

    public void setSourceGrade(String sourceGrade) {
        this.sourceGrade = sourceGrade;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "AppUserPolicy{" +
        "id=" + id +
        ", policyType=" + policyType +
        ", typeDesc=" + typeDesc +
        ", policyContent=" + policyContent +
        ", policyGrade=" + policyGrade +
        ", policyName=" + policyName +
        ", sourceGrade=" + sourceGrade +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        "}";
    }
}
