package com.village.friend.entity;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 分润政策表
 * </p>
 *
 * @author yl
 * @since 2021-08-23
 */
@ApiModel(value="AppProfitPolicy对象", description="分润政策表")
public class AppProfitPolicy implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "功能分组")
    private String colGroup;

    @ApiModelProperty(value = "功能标识")
    private String colKey;

    @ApiModelProperty(value = "功能值")
    private String colVal;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "更新时间")
    private LocalDateTime updateTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getColGroup() {
        return colGroup;
    }

    public void setColGroup(String colGroup) {
        this.colGroup = colGroup;
    }

    public String getColKey() {
        return colKey;
    }

    public void setColKey(String colKey) {
        this.colKey = colKey;
    }

    public String getColVal() {
        return colVal;
    }

    public void setColVal(String colVal) {
        this.colVal = colVal;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "AppProfitPolicy{" +
        "id=" + id +
        ", colGroup=" + colGroup +
        ", colKey=" + colKey +
        ", colVal=" + colVal +
        ", remark=" + remark +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        "}";
    }

    public List<ColValueBean> transKey2String(){
        if(colKey == null){
            return null;
        }
        try {
            return JSON.parseArray(colKey,ColValueBean.class);
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
}
