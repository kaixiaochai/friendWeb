package com.village.friend.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.village.friend.dto.request.*;
import com.village.friend.dto.response.*;
import com.village.friend.entity.User;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

public interface UserService extends IService<User> {

    // 获取用户信息
    BaseResponse<User> getUserMsg(GetUserDto param);
    // 更新用户信息
    BaseResponse<UserResDto> updateUserMsg(UpdateUserDto param);
    // 心跳
    BaseResponse<Object> heart(HeartDto param);
    BaseResponse<KeyWordsResDto> keyWords(KeyWordsDto param);
    // 根据用户id获取用户信息
    List<User> listIn(Collection<String> usernames);
    // 根据用户id获取用户信息
    BaseResponse<IPage<User>> page(UserPageDto param);
    //按邀请码查用户
    User findByInviteCode(String inviteCode);

    //按邀请码查用户
    User findByUsername(String username);

    //查询指定层级的上级
    User findParentByLevel(String username, Integer level);
}
