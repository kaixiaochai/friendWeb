package com.village.friend.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.village.friend.entity.AppUserMapping;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yl
 * @since 2020-09-18
 */
public interface AppUserMappingService extends IService<AppUserMapping> {

    /**
     * 添加一组用户关系
     * @param username
     * @param parantId
     */
    void insertUserMapping(String username, String parantId);

    /**
     * 删除一个用户的关系映射
     * @param username
     */
    void deleteUser(String username);

    /**
     * 查询所有上级
     * @param username
     * @return
     */
    List<AppUserMapping> findParentUsers(String username);

    /**
     * 查询所有下级
     * @param username
     * @return
     */
    List<AppUserMapping> findSubUsers(String username);

    /**
     * 查找指定上级
     * @param username
     * @param level
     * @return
     */
    AppUserMapping findParentUser(String username, Integer level);
}
