package com.village.friend.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.common.utils.StringUtils;
import com.village.friend.dto.request.UserBlackListAddDto;
import com.village.friend.dto.request.UserLikeListDto;
import com.village.friend.entity.AppUserBlackList;
import com.village.friend.entity.User;
import com.village.friend.mapper.AppUserBlackListMapper;
import com.village.friend.service.AppUserBlackListService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.village.friend.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.TreeSet;

/**
 * <p>
 * 用户黑名单表 服务实现类
 * </p>
 *
 * @author yl
 * @since 2021-09-02
 */
@Service
public class AppUserBlackListServiceImpl extends ServiceImpl<AppUserBlackListMapper, AppUserBlackList> implements AppUserBlackListService {

    @Autowired
    UserService userService;

    @Override
    public void black(UserBlackListAddDto param) {
        QueryWrapper<AppUserBlackList> qw = new QueryWrapper<>();
        qw.eq("user_id",param.getUsername());
        qw.eq("black_id",param.getBlackUser());

        if(param.getBlack() == 0){
            remove(qw);
        }else if(param.getBlack() == 1){
            saveOrUpdate(genBlack(param.getUsername(),param.getBlackUser()));
        }
    }

    @Override
    public IPage<User> pageByParam(UserLikeListDto param) {

        QueryWrapper<AppUserBlackList> qw = new QueryWrapper<>();
        if(StringUtils.isNotEmpty(param.getUsername()))
            qw.eq("user_id",param.getUsername());
        IPage<AppUserBlackList> page = new Page<>(param.getPage(), param.getLimit());
        IPage<AppUserBlackList> iPage = baseMapper.selectPage(page, qw);

        List<User> users = new ArrayList<>();
        Collection<String> likes = new TreeSet<>();
        if(iPage.getRecords() != null && iPage.getRecords().size() > 0){
            for (AppUserBlackList like : iPage.getRecords()) {
                likes.add(like.getBlackId());
            }
            users = userService.listIn(likes);
        }


        IPage<User> userIPage = new Page<>();
        userIPage.setTotal(iPage.getTotal());
        userIPage.setPages(iPage.getPages());
        userIPage.setCurrent(iPage.getCurrent());
        userIPage.setSize(iPage.getSize());
        userIPage.setRecords(users);

        return userIPage;
    }

    private AppUserBlackList genBlack(String username, String likeuser){
        AppUserBlackList like = new AppUserBlackList();
        like.setUserId(username);
        like.setBlackId(likeuser);
        return like;
    }
}
