package com.village.friend.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.village.friend.constant.PolicyType;
import com.village.friend.dto.request.ProfitPolicyAddDto;
import com.village.friend.dto.request.ProfitPolicyQueryDto;
import com.village.friend.entity.AppProfitPolicy;
import com.village.friend.mapper.AppProfitPolicyMapper;
import com.village.friend.service.AppProfitPolicyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 分润政策表 服务实现类
 * </p>
 *
 * @author yl
 * @since 2021-08-23
 */
@Service
public class AppProfitPolicyServiceImpl extends ServiceImpl<AppProfitPolicyMapper, AppProfitPolicy> implements AppProfitPolicyService {

    @Override
    public AppProfitPolicy findByGroupAndKey(String group, String key) {
        QueryWrapper<AppProfitPolicy> qw = new QueryWrapper<>();
        qw.eq("col_group",group);
        qw.eq("col_key",key);
        return baseMapper.selectOne(qw);
    }

    @Override
    public void addUpgradePolicy(ProfitPolicyAddDto param) {
        AppProfitPolicy profitPolicy = new AppProfitPolicy();
        profitPolicy.setColGroup(PolicyType.UPGRADE.getType());
        profitPolicy.setColKey(param.getGrade() + "");
        profitPolicy.setColVal(JSON.toJSONString(param.getList()));
        save(profitPolicy);
    }

    @Override
    public List<AppProfitPolicy> queryList(ProfitPolicyQueryDto param) {
        QueryWrapper<AppProfitPolicy> qw = new QueryWrapper<>();
        if(param.getId() != null) {
            qw.eq("id", param.getId());
        }
        if(StringUtils.isNotBlank(param.getColGroup())) {
            qw.eq("col_group",param.getColGroup());
        }
        if(StringUtils.isNotBlank(param.getColKey())) {
            qw.eq("col_key",param.getColKey());
        }
        return baseMapper.selectList(qw);
    }
}
