package com.village.friend.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.utils.StringUtils;
import com.village.friend.dto.request.AppInviteRequestDto;
import com.village.friend.entity.AppInvite;
import com.village.friend.mapper.AppInviteMapper;
import com.village.friend.service.AppInviteService;
import com.village.friend.utils.InviteUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yl
 * @since 2021-05-24
 */
@Service
public class AppInviteServiceImpl extends ServiceImpl<AppInviteMapper, AppInvite> implements AppInviteService {

    @Override
    public List<AppInvite> findByInviteCodes(Collection<String> codes) {
        QueryWrapper<AppInvite> qw = new QueryWrapper<>();
        qw.in("invite_code",codes);
        return baseMapper.selectList(qw);
    }

    @Override
    public AppInvite findOneDotUseInviteCode() {
        AppInvite invite = baseMapper.findOneDotUseInviteCode();
        //如果没有可用的邀请码,自动生成
        if(invite == null){
            List<AppInvite> appInvites = genInviteList(50);
            if(appInvites.size() > 0){
                saveBatch(appInvites);
                return appInvites.get(0);
            }
            throw new RuntimeException("无可用邀请码,生成新邀请码失败!!");
        }
        return invite;
    }

    @Override
    public AppInvite findByInviteCode(String inviteCode) {
        QueryWrapper<AppInvite> qw = new QueryWrapper<>();
        qw.eq("invite_code",inviteCode);
        return baseMapper.selectOne(qw);
    }

    @Override
    public boolean updateUsed(String inviteCode) {
        AppInvite invite = findByInviteCode(inviteCode);
        invite.setIsused("1");
        return updateById(invite);
    }

    @Override
    public int genNewInviteCodes(int count) {
        List<AppInvite> list = genInviteList(count);
        if(list.size() > 0) {
            saveBatch(list);
        }
        return list.size();
    }

    @Override
    public IPage<AppInvite> queryPage(AppInviteRequestDto param) {
        IPage<AppInvite> page = new Page<>(param.getPage(), param.getLimit());
        QueryWrapper<AppInvite> qw = new QueryWrapper<>();
        if(StringUtils.isNotEmpty(param.getInviteCode())) {
            qw.eq("id", param.getId());
        }
        if(StringUtils.isNotEmpty(param.getInviteCode())) {
            qw.eq("invite_code", param.getInviteCode());
        }
        if(param.getIsused() != null) {
            qw.eq("isused", param.getIsused());
        }

        if(org.apache.commons.lang.StringUtils.isNotEmpty(param.getStartTime())){
            qw.ge("create_time",param.getStartTime());
        }
        if(org.apache.commons.lang.StringUtils.isNotEmpty(param.getEndTime())){
            qw.le("create_time",param.getEndTime());
        }
        qw.orderByDesc("create_time");
        return super.page(page,qw);
    }

    /**
     * 生成指定数量的邀请码
     * @param count
     * @return
     */
    private List<AppInvite> genInviteList(int count){
        //生成对应数量的code
        List<String> codes = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            String code = InviteUtils.getInviteCode();
            if(!codes.contains(code)){
                codes.add(code);
            }else {
                i -- ;
            }
        }

        //过滤掉重复的code
        List<AppInvite> hasList = findByInviteCodes(codes);
        if(hasList != null && hasList.size() > 0){
            for (int i = 0; i < hasList.size(); i++) {
                AppInvite invite = hasList.get(i);
                codes.remove(invite.getInviteCode());
            }
        }

        //生成要插入的数据
        List<AppInvite> list = new ArrayList<>();
        for (int i = 0; i < codes.size(); i++) {
            AppInvite invitepo = new AppInvite();
            invitepo.setInviteCode(InviteUtils.getInviteCode());
            invitepo.setIsused("0");
            list.add(invitepo);
        }
        return list;
    }
}
