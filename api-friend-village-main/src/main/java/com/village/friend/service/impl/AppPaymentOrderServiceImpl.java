package com.village.friend.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.village.friend.dto.request.AppPaymentOrderQueryDto;
import com.village.friend.entity.AppPaymentOrder;
import com.village.friend.mapper.AppPaymentOrderMapper;
import com.village.friend.service.AppPaymentOrderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 交易订单 服务实现类
 * </p>
 *
 * @author yl
 * @since 2021-08-23
 */
@Service
public class AppPaymentOrderServiceImpl extends ServiceImpl<AppPaymentOrderMapper, AppPaymentOrder> implements AppPaymentOrderService {

    @Override
    public IPage<AppPaymentOrder> pageByParam(AppPaymentOrderQueryDto param) {
        IPage<AppPaymentOrder> page = new Page<>(param.getPage(),param.getLimit());
        QueryWrapper<AppPaymentOrder> qw = new QueryWrapper<>();
        if(StringUtils.isNotBlank(param.getOrderNo())){
            qw.eq("id",param.getOrderNo());
        }
        if(StringUtils.isNotBlank(param.getPayStatus())){
            qw.eq("pay_status",param.getPayStatus());
        }
        if(StringUtils.isNotBlank(param.getPayTypeNo())){
            qw.eq("pay_type_no",param.getPayTypeNo());
        }
        if(StringUtils.isNotBlank(param.getPayWayName())){
            qw.eq("pay_way_name",param.getPayWayName());
        }
        if(StringUtils.isNotBlank(param.getUsername())){
            qw.eq("user_id",param.getUsername());
        }
        IPage<AppPaymentOrder> orderIPage = super.page(page, qw);
        return orderIPage;
    }
}
