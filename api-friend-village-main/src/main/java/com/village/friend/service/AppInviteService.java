package com.village.friend.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.village.friend.dto.request.AppInviteRequestDto;
import com.village.friend.entity.AppInvite;

import java.util.Collection;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yl
 * @since 2021-05-24
 */
public interface AppInviteService extends IService<AppInvite> {

    /**
     * 获取指定邀请码
     * @param codes
     * @return
     */
    List<AppInvite> findByInviteCodes(Collection<String> codes);

    /**
     * 获取一个未使用的邀请码
     * @param inviteCode
     * @return
     */
    AppInvite findByInviteCode(String inviteCode);

    /**
     * 获取一个未使用的邀请码
     * @return
     */
    AppInvite findOneDotUseInviteCode();

    /**
     * 设为已使用
     * @param inviteCode
     * @return
     */
    boolean updateUsed(String inviteCode);

    /**
     * 生成新的邀请码
     * @param count
     * @return
     */
    int genNewInviteCodes(int count);

    /**
     * 分页查询
     * @param param
     * @return
     */
    IPage<AppInvite> queryPage(AppInviteRequestDto param);
}
