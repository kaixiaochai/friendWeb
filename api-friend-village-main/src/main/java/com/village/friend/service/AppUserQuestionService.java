package com.village.friend.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.village.friend.dto.request.QuestionAddDto;
import com.village.friend.dto.request.QuestionListDto;
import com.village.friend.entity.AppUserQuestion;

/**
 * <p>
 * 用户反馈表 服务类
 * </p>
 *
 * @author yl
 * @since 2021-09-23
 */
public interface AppUserQuestionService extends IService<AppUserQuestion> {

    void add(QuestionAddDto param);

    void del(Integer id);

    IPage<AppUserQuestion> pageByParam(QuestionListDto params);
}
