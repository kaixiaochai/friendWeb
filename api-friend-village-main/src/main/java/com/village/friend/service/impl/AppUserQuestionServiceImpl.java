package com.village.friend.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.utils.StringUtils;
import com.village.friend.dto.request.QuestionAddDto;
import com.village.friend.dto.request.QuestionListDto;
import com.village.friend.entity.AppUserQuestion;
import com.village.friend.mapper.AppUserQuestionMapper;
import com.village.friend.service.AppUserQuestionService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户反馈表 服务实现类
 * </p>
 *
 * @author yl
 * @since 2021-09-23
 */
@Service
public class AppUserQuestionServiceImpl extends ServiceImpl<AppUserQuestionMapper, AppUserQuestion> implements AppUserQuestionService {

    @Override
    public void add(QuestionAddDto param) {
        AppUserQuestion question = new AppUserQuestion();
        question.setQuestion(param.getQuestion());
        question.setUserId(param.getUsername());
        question.setContact(param.getContact());
        question.setImages(JSON.toJSONString(param.getImageList()));
        save(question);
    }

    @Override
    public void del(Integer id) {
        AppUserQuestion q = getById(id);
        if(q == null){
            throw new RuntimeException("要删除的问题不存在");
        }
        removeById(id);
    }

    @Override
    public IPage<AppUserQuestion> pageByParam(QuestionListDto params) {
        QueryWrapper<AppUserQuestion> qw = new QueryWrapper<>();
        if(StringUtils.isNotEmpty(params.getUsername())) {
            qw.eq("user_id", params.getUsername());
        }
        if(params.getId() != null){
            qw.eq("id", params.getId());
        }

        IPage<AppUserQuestion> page = new Page<>(params.getPage(), params.getLimit());
        return baseMapper.selectPage(page, qw);
    }
}
