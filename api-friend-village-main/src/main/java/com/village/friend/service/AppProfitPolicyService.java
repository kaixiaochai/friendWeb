package com.village.friend.service;

import com.village.friend.dto.request.ProfitPolicyAddDto;
import com.village.friend.dto.request.ProfitPolicyQueryDto;
import com.village.friend.entity.AppProfitPolicy;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 分润政策表 服务类
 * </p>
 *
 * @author yl
 * @since 2021-08-23
 */
public interface AppProfitPolicyService extends IService<AppProfitPolicy> {

    AppProfitPolicy findByGroupAndKey(String group, String key);

    void addUpgradePolicy(ProfitPolicyAddDto param);

    List<AppProfitPolicy> queryList(ProfitPolicyQueryDto param);
}
