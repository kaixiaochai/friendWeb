package com.village.friend.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.village.friend.dto.request.AppPaymentOrderQueryDto;
import com.village.friend.entity.AppPaymentOrder;
import com.baomidou.mybatisplus.extension.service.IService;
import com.village.friend.entity.AppUserProfit;

import java.util.Map;

/**
 * <p>
 * 交易订单 服务类
 * </p>
 *
 * @author yl
 * @since 2021-08-23
 */
public interface AppPaymentOrderService extends IService<AppPaymentOrder> {

    IPage<AppPaymentOrder> pageByParam(AppPaymentOrderQueryDto param);
}
