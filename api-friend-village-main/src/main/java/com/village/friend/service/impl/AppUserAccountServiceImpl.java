package com.village.friend.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.village.friend.dto.request.AccountDto;
import com.village.friend.entity.AppUserAccount;
import com.village.friend.mapper.AppUserAccountMapper;
import com.village.friend.service.AppUserAccountService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.village.friend.utils.ParamUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户账户表  服务实现类
 * </p>
 *
 * @author yl
 * @since 2021-08-21
 */
@Service
public class AppUserAccountServiceImpl extends ServiceImpl<AppUserAccountMapper, AppUserAccount> implements AppUserAccountService {

    @Override
    public AppUserAccount findByUserId(String userId) {
        QueryWrapper<AppUserAccount> qw = new QueryWrapper<>();
        qw.eq("user_id",userId);
        return baseMapper.selectOne(qw);
    }

    @Override
    public IPage<AppUserAccount> queryPage(AccountDto param) {
        IPage<AppUserAccount> page = new Page<>(param.getPage(), param.getLimit());
        QueryWrapper<AppUserAccount> qw = new QueryWrapper<>();
        if(StringUtils.isNotEmpty(param.getId())) {
            qw.eq("id", param.getId());
        }
        if(StringUtils.isNotEmpty(param.getUserId())) {
            qw.eq("user_id", param.getUserId());
        }

        ParamUtils.fullTime(param,qw);
        qw.orderByDesc("create_time");
        return super.page(page,qw);
    }
}
