package com.village.friend.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.village.friend.dto.request.UserBlackListAddDto;
import com.village.friend.dto.request.UserLikeListDto;
import com.village.friend.entity.AppUserBlackList;
import com.baomidou.mybatisplus.extension.service.IService;
import com.village.friend.entity.User;

/**
 * <p>
 * 用户黑名单表 服务类
 * </p>
 *
 * @author yl
 * @since 2021-09-02
 */
public interface AppUserBlackListService extends IService<AppUserBlackList> {

    void black(UserBlackListAddDto param);

    IPage<User> pageByParam(UserLikeListDto param);
}
