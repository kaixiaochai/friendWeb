package com.village.friend.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.village.friend.entity.UserHeart;
import com.village.friend.mapper.UserHeartMapper;
import com.village.friend.service.UserHeartService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * <p>
 * 用户心跳 服务实现类
 * </p>
 *
 * @author yl
 * @since 2021-08-13
 */
@Service
public class UserHeartServiceImpl extends ServiceImpl<UserHeartMapper, UserHeart> implements UserHeartService {

    @Override
    public Integer insertOrUpdate(String username, String ip, Double latitude, Double longitude) {
        return baseMapper.add(username,ip,latitude,longitude);
    }

    @Override
    public List<UserHeart> listInUsername(Collection<String> usernames) {
        List<UserHeart> resultList = new ArrayList<>();
        QueryWrapper<UserHeart> ew = new QueryWrapper<>();

        if(usernames == null || usernames.isEmpty()){
            return resultList;
        }
        if(usernames.size() == 1 ){
            ew.eq("username",usernames.toArray()[0]);
        }else {
            ew.in("username",usernames);
        }
        return baseMapper.selectList(ew);
    }
}
