package com.village.friend.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.village.friend.dto.request.ActivityListDto;
import com.village.friend.dto.response.ActivityListResDto;
import com.village.friend.entity.Activity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yl
 * @since 2021-08-13
 */
public interface ActivityService extends IService<Activity> {

    IPage<Activity> pageByParam(ActivityListDto param);

}
