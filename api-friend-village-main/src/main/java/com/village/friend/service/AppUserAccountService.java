package com.village.friend.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.village.friend.dto.request.AccountDto;
import com.village.friend.entity.AppUserAccount;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户账户表  服务类
 * </p>
 *
 * @author yl
 * @since 2021-08-21
 */
public interface AppUserAccountService extends IService<AppUserAccount> {

    AppUserAccount findByUserId(String userId);

    IPage<AppUserAccount> queryPage(AccountDto param);
}
