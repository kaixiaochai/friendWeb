package com.village.friend.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.village.friend.dto.request.UserLikeAddDto;
import com.village.friend.dto.request.UserLikeListDto;
import com.village.friend.dto.response.ActivityListResDto;
import com.village.friend.entity.AppUserLike;
import com.baomidou.mybatisplus.extension.service.IService;
import com.village.friend.entity.User;

/**
 * <p>
 * 用户关注表 服务类
 * </p>
 *
 * @author yl
 * @since 2021-09-01
 */
public interface AppUserLikeService extends IService<AppUserLike> {

    void like(UserLikeAddDto param);

    IPage<User> pageByParam(UserLikeListDto param);
}
