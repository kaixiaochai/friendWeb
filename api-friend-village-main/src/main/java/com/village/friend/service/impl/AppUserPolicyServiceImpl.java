package com.village.friend.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.village.friend.constant.PolicyType;
import com.village.friend.dto.request.PolicyAddDto;
import com.village.friend.dto.request.PolicyInitDto;
import com.village.friend.dto.request.PolicyListDto;
import com.village.friend.dto.request.PolicyUpdateDto;
import com.village.friend.entity.AppUserPolicy;
import com.village.friend.mapper.AppUserPolicyMapper;
import com.village.friend.service.AppUserPolicyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 会员缴费政策表 服务实现类
 * </p>
 *
 * @author yl
 * @since 2021-08-23
 */
@Service
public class AppUserPolicyServiceImpl extends ServiceImpl<AppUserPolicyMapper, AppUserPolicy> implements AppUserPolicyService {

    //没有原价
    private  final int defaultAmout = 68;
    @Override
    public void initPolicy(PolicyInitDto param) {
        List<AppUserPolicy> list = new ArrayList<>();
        list.add(genPolicy(0,1,param.getMonth1()));
        list.add(genPolicy(0,3,param.getMonth3()));
        list.add(genPolicy(0,6,param.getMonth6()));
        list.add(genPolicy(0,12,param.getMonth12()));
        saveOrUpdateBatch(list);
    }

    @Override
    public void addPolicy(PolicyAddDto param) {
        Integer grade = param.getTime();
        AppUserPolicy policy = genPolicy(0, grade, param.getAmount());
        baseMapper.insert(policy);
    }

    @Override
    public void updatePolicy(PolicyUpdateDto param) {
        QueryWrapper<AppUserPolicy> qw = new QueryWrapper<>();
        qw.eq("policy_type",param.getPolicyType());
        qw.eq("policy_grade",param.getPolicyGrade());
        qw.eq("source_grade",param.getSourceGrade());
        AppUserPolicy policy = baseMapper.selectOne(qw);
        if(policy == null){
            throw new RuntimeException("找不到满足条件的策略");
        }
        policy.setPolicyContent(param.getPolicyContent());
        updateById(policy);
    }

    @Override
    public List<AppUserPolicy> queryParam(PolicyListDto param) {
        QueryWrapper<AppUserPolicy> qw = new QueryWrapper<>();
        qw.eq("policy_type",param.getPolicyType());
        if(param.getTime() != null){
            qw.eq("policy_grade",param.getTime());
        }
        qw.orderByAsc("source_grade");
        return list(qw);
    }

    @Override
    public AppUserPolicy findOneByTypeAndGrade(String type, Integer grade) {
        QueryWrapper<AppUserPolicy> qw = new QueryWrapper<>();
        qw.eq("policy_type",type);
        qw.eq("policy_grade",grade);
        return getOne(qw);
    }

    @Override
    public AppUserPolicy findOneByTypeAndAmount(String type, Integer amount) {
        QueryWrapper<AppUserPolicy> qw = new QueryWrapper<>();
        qw.eq("policy_type",type);
        qw.eq("policy_content",amount);
        return getOne(qw);
    }

    /**
     * 生成用户升级策略配置
     * @param org
     * @param dest
     * @param amount
     * @return
     */
    private AppUserPolicy genPolicy(Integer org, Integer dest, Integer amount){
        int yhAmount = defaultAmout * dest;
        AppUserPolicy policy = new AppUserPolicy();
        policy.setPolicyType(PolicyType.UPGRADE.getType());
        policy.setTypeDesc(PolicyType.UPGRADE.getName());
        policy.setPolicyName(PolicyType.UPGRADE.getName() + dest + "个月" );
        policy.setSourceGrade(org.toString());
        policy.setPolicyGrade(dest.toString());
        policy.setPolicyContent(amount.toString());

        return policy;
    }

}
