package com.village.friend.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.village.friend.dto.request.ListDto;
import com.village.friend.dto.response.BaseResponse;
import com.village.friend.dto.response.UserResDto;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface HomeService {
    IPage<UserResDto> list(ListDto param);
}
