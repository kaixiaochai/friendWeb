package com.village.friend.service;

import com.village.friend.dto.request.PolicyAddDto;
import com.village.friend.dto.request.PolicyInitDto;
import com.village.friend.dto.request.PolicyListDto;
import com.village.friend.dto.request.PolicyUpdateDto;
import com.village.friend.entity.AppUserPolicy;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 会员缴费政策表 服务类
 * </p>
 *
 * @author yl
 * @since 2021-08-23
 */
public interface AppUserPolicyService extends IService<AppUserPolicy> {

    void initPolicy(PolicyInitDto param);

    void updatePolicy(PolicyUpdateDto param);

    void addPolicy(PolicyAddDto param);

    List<AppUserPolicy> queryParam(PolicyListDto param);

    AppUserPolicy findOneByTypeAndGrade(String type, Integer grade);

    AppUserPolicy findOneByTypeAndAmount(String type, Integer amout);
}
