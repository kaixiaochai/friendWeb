package com.village.friend.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.village.friend.dto.request.ActivityListDto;
import com.village.friend.dto.response.ActivityListResDto;
import com.village.friend.entity.Activity;
import com.village.friend.mapper.ActivityMapper;
import com.village.friend.service.ActivityService;
import com.village.friend.utils.ParamUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author yl
 * @since 2021-08-13
 */
@Service
public class ActivityServiceImpl extends ServiceImpl<ActivityMapper, Activity> implements ActivityService {

    @Override
    public IPage<Activity> pageByParam(ActivityListDto param) {
        IPage<Activity> page = new Page<>(param.getPage(), param.getLimit());
        QueryWrapper<Activity> qw = new QueryWrapper<>();
        if(param.getType() != null) {
            qw.eq("type", param.getType());
        }
        if(StringUtils.isNotBlank(param.getCity())) {
            qw.eq("city", param.getCity());
        }
        if(StringUtils.isNotBlank(param.getUsername())) {
            qw.eq("username", param.getUsername());
        }

        return super.page(page);
    }
}
