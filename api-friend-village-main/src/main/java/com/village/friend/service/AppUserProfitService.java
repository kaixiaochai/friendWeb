package com.village.friend.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.village.friend.dto.request.AppUserProfitQueryDto;
import com.village.friend.entity.AppPaymentOrder;
import com.village.friend.entity.AppUserProfit;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户收益明细表 服务类
 * </p>
 *
 * @author yl
 * @since 2021-08-23
 */
public interface AppUserProfitService extends IService<AppUserProfit> {

    /**
     * 查询订单分润信息
     * @param oderNo
     * @return
     */
    List<AppUserProfit> findByOrderNo(String oderNo);

    /**
     * 分润
     * @param order
     */
    void saveMemberProfit(AppPaymentOrder order);

    IPage<AppUserProfit> pageByParam(AppUserProfitQueryDto param);
}
