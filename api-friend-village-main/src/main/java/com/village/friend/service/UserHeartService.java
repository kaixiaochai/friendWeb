package com.village.friend.service;

import com.village.friend.entity.UserHeart;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Collection;
import java.util.List;

/**
 * <p>
 * 用户心跳 服务类
 * </p>
 *
 * @author yl
 * @since 2021-08-13
 */
public interface UserHeartService extends IService<UserHeart> {

    Integer insertOrUpdate(String username, String ip, Double latitude, Double longitude);

    List<UserHeart> listInUsername(Collection<String> usernames);

}
