package com.village.friend.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.village.friend.dto.request.*;
import com.village.friend.dto.response.*;
import com.village.friend.entity.User;
import com.village.friend.service.impl.UserServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
@Api(tags = "用户模块")
public class UserController extends BaseController {
    @Autowired
    private UserServiceImpl userService;

    @ApiOperation(value = "获取用户信息")
    @GetMapping(value = "/getUser")
    public BaseResponse<User> getUser(GetUserDto param) {
        return userService.getUserMsg(param);
    }

    @ApiOperation(value = "更新用户信息", httpMethod = "POST")
    @RequestMapping(value = "/updateUser", method = RequestMethod.POST)
    public BaseResponse<UserResDto> updateUser(@RequestBody UpdateUserDto param) {
        return userService.updateUserMsg(param);
    }

    @ApiOperation(value = "用户心跳", httpMethod = "POST")
    @RequestMapping(value = "/heart", method = RequestMethod.POST)
    public BaseResponse<Object> heart(@RequestBody HeartDto param) {
        return userService.heart(param);
    }

    @ApiOperation(value = "关键词:对活动的要求,关于我,对爱的看法")
    @GetMapping(value = "/keyWords")
    public BaseResponse<KeyWordsResDto> keyWords(KeyWordsDto param) {
        return userService.keyWords(param);
    }

    @ApiOperation(value = "分页查询")
    @GetMapping(value = "/page")
    public BaseResponse<IPage<User>> page(UserPageDto param) {
        return userService.page(param);
    }


}
