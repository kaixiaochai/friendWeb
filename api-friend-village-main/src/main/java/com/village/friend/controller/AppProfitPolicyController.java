package com.village.friend.controller;


import com.village.friend.dto.request.ProfitPolicyAddDto;
import com.village.friend.dto.request.ProfitPolicyDeleteDto;
import com.village.friend.dto.request.ProfitPolicyQueryDto;
import com.village.friend.dto.response.BaseResponse;
import com.village.friend.entity.AppProfitPolicy;
import com.village.friend.service.AppProfitPolicyService;
import com.village.friend.utils.GlobalUtils;
import com.village.friend.utils.ParamUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 分润政策表 前端控制器
 * </p>
 *
 * @author yl
 * @since 2021-08-23
 */
@RestController
@RequestMapping("/appProfitPolicy")
@Api(tags = "分润策略管理")
public class AppProfitPolicyController {

    @Autowired
    private AppProfitPolicyService service;

    @ApiOperation(value = "添加升级分润配置")
    @PostMapping("/addUpgrade")
    public BaseResponse<String> addUpgradePolicy(@RequestBody ProfitPolicyAddDto param){
        if(param == null || GlobalUtils.checkIsNull(param.getGrade(),param.getList())){
            return BaseResponse.paramsError();
        }
        if(param.getList().isEmpty()){
            return BaseResponse.paramsError();
        }
        //检查配置是否合法
        if(!ParamUtils.checkProfit(param.getList())){
            return BaseResponse.paramsError("分润配置异常,比例配置过高或小于0!!");
        }
        try {
            service.addUpgradePolicy(param);
            return BaseResponse.success("成功添加");
        }catch (Exception e){
            e.printStackTrace();
            return BaseResponse.error(e.getMessage());
        }

    }

    @ApiOperation(value = "删除策略")
    @PostMapping("/delete")
    public BaseResponse<String> delete(@RequestBody ProfitPolicyDeleteDto param){
        if(param == null || GlobalUtils.checkIsNull(param.getId())){
            return BaseResponse.paramsError();
        }
        try {
            service.removeById(param.getId());
            return BaseResponse.success("删除成功");
        }catch (Exception e){
            e.printStackTrace();
            return BaseResponse.error(e.getMessage());
        }

    }

    @ApiOperation(value = "查询分润策略")
    @GetMapping("/list")
    public BaseResponse<List<AppProfitPolicy>> list(ProfitPolicyQueryDto param){
        if(param == null){
            return BaseResponse.paramsError();
        }
        try {
            return BaseResponse.success(service.queryList(param));
        }catch (Exception e){
            e.printStackTrace();
            return BaseResponse.error(e.getMessage());
        }

    }

}


