package com.village.friend.controller;


import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.village.friend.constant.PayStatus;
import com.village.friend.constant.PolicyType;
import com.village.friend.dto.request.AppPaymentOrderQueryDto;
import com.village.friend.dto.request.UpgradePayDto;
import com.village.friend.dto.response.BaseResponse;
import com.village.friend.entity.AppPaymentOrder;
import com.village.friend.entity.AppUserPolicy;
import com.village.friend.entity.User;
import com.village.friend.service.AppPaymentOrderService;
import com.village.friend.service.AppUserPolicyService;
import com.village.friend.service.UserService;
import com.village.friend.utils.GlobalUtils;
import com.village.friend.utils.OrderUtils;
import com.village.friend.utils.ParamUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 交易订单 前端控制器
 * </p>
 *
 * @author yl
 * @since 2021-08-23
 */
@RestController
@RequestMapping("/appPaymentOrder")
@Slf4j
@Api(tags = "交易订单管理")
public class AppPaymentOrderController {


    @Autowired
    private AppPaymentOrderService service;
    @Autowired
    private UserService appUserService;
    @Autowired
    private AppUserPolicyService policyService;

    @ApiOperation(value = "分页查询")
    @GetMapping("/page")
    public BaseResponse<IPage<AppPaymentOrder>> page(AppPaymentOrderQueryDto param){

        if(param == null){
            return BaseResponse.paramsError();
        }
        try {
            IPage<AppPaymentOrder> page = service.pageByParam(param);
            return BaseResponse.success(page);
        }catch (Exception e){
            e.printStackTrace();
            return BaseResponse.error(e.getMessage());
        }
    }




    private void genAndSaveOrder(User user, String orderNo, String payType, String amount){
        AppPaymentOrder order = ParamUtils.genOrder(user, payType, orderNo, PolicyType.UPGRADE, BigDecimal.valueOf(Double.valueOf(amount)), "会员升级缴费", PayStatus.IN_PROCESS);
        service.saveOrUpdate(order);
    }
}

