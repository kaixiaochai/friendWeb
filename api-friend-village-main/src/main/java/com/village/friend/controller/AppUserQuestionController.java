package com.village.friend.controller;


import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.village.friend.constant.MsgCodeEnum;
import com.village.friend.dto.request.QuestionAddDto;
import com.village.friend.dto.request.QuestionDelDto;
import com.village.friend.dto.request.QuestionListDto;
import com.village.friend.dto.response.BaseResponse;
import com.village.friend.entity.AppUserQuestion;
import com.village.friend.entity.User;
import com.village.friend.service.AppUserQuestionService;
import com.village.friend.utils.GlobalUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * <p>
 * 用户反馈表 前端控制器
 * </p>
 *
 * @author yl
 * @since 2021-09-23
 */
@RestController
@RequestMapping("/appUserQuestion")
@Slf4j
@Api(tags = "用户问题反馈管理")
public class AppUserQuestionController {

    @Autowired
    AppUserQuestionService service;



    @ApiOperation(value = "问题反馈")
    @PostMapping(value = "/add")
    public BaseResponse<String> add(@RequestBody QuestionAddDto param) {
        if (param == null || StringUtils.isBlank(param.getQuestion())) {
            return BaseResponse.paramsError();
        }
        try {


            service.add(param);
            return BaseResponse.success();
        }catch (Exception e){
            e.printStackTrace();
            return BaseResponse.error(e.getMessage());
        }

    }


    @ApiOperation(value = "删除问题反馈")
    @PostMapping(value = "/delete")
    public BaseResponse<String> delete(@RequestBody QuestionDelDto param) {
        if (param == null || null == param.getId()) {
            return BaseResponse.paramsError();
        }
        try {


            service.del(param.getId());
            return BaseResponse.success();
        }catch (Exception e){
            e.printStackTrace();
            return BaseResponse.error(e.getMessage());
        }

    }


    @ApiOperation(value = "分页查询")
    @GetMapping("/page")
    public BaseResponse<IPage<AppUserQuestion>> userPage(QuestionListDto param) {
        if (param == null) {
            return BaseResponse.paramsError();
        }
        try {
            //拉取分页数据
            IPage<AppUserQuestion> data = service.pageByParam(param);
            return new BaseResponse(MsgCodeEnum.SUCCESS, data);
        }catch (Exception e){
            e.printStackTrace();
            return BaseResponse.error(e.getMessage());
        }
    }

}

