package com.village.friend.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.village.friend.dto.request.UserBlackListAddDto;
import com.village.friend.dto.request.UserLikeAddDto;
import com.village.friend.dto.request.UserLikeListDto;
import com.village.friend.dto.response.BaseResponse;
import com.village.friend.entity.User;
import com.village.friend.service.AppUserBlackListService;
import com.village.friend.service.AppUserLikeService;
import com.village.friend.service.impl.UserServiceImpl;
import com.village.friend.utils.GlobalUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 用户黑名单表 前端控制器
 * </p>
 *
 * @author yl
 * @since 2021-09-02
 */
@RestController
@RequestMapping("/appUserBlackList")
@Api(tags = "用户黑名单模块")
public class AppUserBlackListController {

    @Autowired
    private UserServiceImpl userService;
    @Autowired
    private AppUserBlackListService service;

    @ApiOperation(value = "添加或删除黑名单")
    @PostMapping(value = "/black")
    public BaseResponse<String> black(@RequestBody UserBlackListAddDto param) {
        if (param == null || GlobalUtils.checkIsNull(param.getBlack(), param.getBlackUser(), param.getUsername())) {
            return BaseResponse.paramsError();
        }
        if(param.getBlack() != 0 && param.getBlack() != 1){
            return BaseResponse.paramsError("black参数不对!!");
        }
        if(GlobalUtils.checkUser(userService,param.getUsername())){
            return BaseResponse.error("用户不存在");
        }
        if(GlobalUtils.checkUser(userService,param.getBlackUser())){
            return BaseResponse.error("要操作的用户不存在");
        }

        try {
            service.black(param);
            return BaseResponse.success();
        } catch (Exception e) {
            e.printStackTrace();
            return BaseResponse.error(e.getMessage());
        }
    }


    @ApiOperation(value = "分页查询")
    @GetMapping("/page")
    public BaseResponse<IPage<User>> userPage(UserLikeListDto param) {
        if (param == null) {
            return BaseResponse.paramsError();
        }
        try {
            IPage<User> data = service.pageByParam(param);
            return BaseResponse.success(data);
        }catch (Exception e){
            e.printStackTrace();
            return BaseResponse.error(e.getMessage());
        }
    }
}

