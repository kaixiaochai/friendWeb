package com.village.friend.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.village.friend.constant.MsgCodeEnum;
import com.village.friend.constant.TimeType;
import com.village.friend.dto.request.ActivityAddDto;
import com.village.friend.dto.request.ActivityJoinDto;
import com.village.friend.dto.request.ActivityListDto;
import com.village.friend.dto.response.ActivityListResDto;
import com.village.friend.dto.response.BaseResponse;
import com.village.friend.entity.Activity;
import com.village.friend.service.ActivityService;
import com.village.friend.service.UserHeartService;
import com.village.friend.service.impl.UserServiceImpl;
import com.village.friend.utils.GlobalUtils;
import com.village.friend.utils.ParamUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.*;

/**
 * <p>
 *
 * </p>
 *
 * @author yl
 * @since 2021-08-13
 */
@Api(tags = "活动管理")
@RestController
@RequestMapping("/activity")
public class ActivityController extends BaseController {

    @Autowired
    public ActivityService activityService;

    @Autowired
    public UserServiceImpl userService;

    @Autowired
    public UserHeartService heartService;


    @ApiOperation("增加活动")
    @PostMapping("/add")
    public BaseResponse<String> add(@RequestBody @Validated ActivityAddDto param) {
        if (param == null || GlobalUtils.checkIsNull(param.getType(), param.getDestType(), param.getCity(), param.getTimeType())) {
            return BaseResponse.paramsError();
        }
        //具体时间 需要判断是否传递日期过期
        if (param.getTimeType() == TimeType.SPECTIME.getCode() && param.getTime() == null) {
            BaseResponse<String> error = BaseResponse.paramsError();
            error.setMsg("请选择具体时间");
            return error;
        }
        // 请求实体 转为 数据库实体
        Activity entity = new Activity();
        entity.setCity(param.getCity());
        entity.setUsername(param.getUsername());
        entity.setType(param.getType());
        entity.setWechat(param.getWechat());
        entity.setTimeType(param.getTimeType());
        entity.setDestType(param.getDestType());
        entity.setTime(param.getTime());
        entity.setInstr(param.getInstr());
        entity.setImages(ParamUtils.toJSONString(param.getImages()));
        try {
            activityService.save(entity);
            return BaseResponse.success();
        } catch (Exception e) {
            e.printStackTrace();
            return BaseResponse.error(e.getMessage());
        }

    }

    @ApiOperation("报名")
    @PostMapping("/join")
    public BaseResponse<String> join(@RequestBody ActivityJoinDto param) {
        if (param == null
                || GlobalUtils.checkIsNull(param.getId()
                , param.getUsername(), param.getIsJoin())) {
            return BaseResponse.paramsError();
        }
        Activity activity = activityService.getById(param.getId());

        if (activity == null) {
            return BaseResponse.error("活动不存在");
        }

        if (param.getUsername().equals(activity.getUsername())) {
            return BaseResponse.error("自己创建的活动不需要报名");
        }
        List<String> joins = ParamUtils.strToList(activity.getJoinUids());
        if (param.getIsJoin() == 0) {
            if (joins != null) {
                joins.remove(param.getUsername());
            }
        } else if (param.getIsJoin() == 1) {
            if (!joins.contains(param.getUsername())) {
                joins.add(param.getUsername());
            }
        } else {
            return BaseResponse.error("错误参数isJoin");
        }

        try {
            Activity update = new Activity();
            update.setId(activity.getId());
            update.setUpdateTime(LocalDateTime.now());
            update.setJoinUids(ParamUtils.toString(joins));
            activityService.updateById(update);
            return BaseResponse.success();
        } catch (Exception e) {
            e.printStackTrace();
            return BaseResponse.error(e.getMessage());
        }

    }

    @ApiOperation(value = "分页查询")
    @GetMapping("/page")
    public BaseResponse<IPage<Activity>> userPage(ActivityListDto param) {
        if (param == null ) {
            return BaseResponse.paramsError();
        }
        IPage<Activity> data = activityService.pageByParam(param);
        return new BaseResponse(MsgCodeEnum.SUCCESS, data);
    }

}

