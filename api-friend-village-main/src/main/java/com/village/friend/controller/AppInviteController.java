package com.village.friend.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.village.friend.dto.request.AppInviteAddDto;
import com.village.friend.dto.request.AppInviteRequestDto;
import com.village.friend.dto.response.BaseResponse;
import com.village.friend.entity.AppInvite;
import com.village.friend.service.AppInviteService;
import com.village.friend.utils.GlobalUtils;
import com.village.friend.utils.InviteUtils;
import com.village.friend.utils.ParamUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author yl
 * @since 2021-08-17
 */
@RestController
@RequestMapping("/appInvite")
@Api(tags = "邀请码管理")
public class AppInviteController {

    @Autowired
    private AppInviteService appInviteService;

    @ApiOperation(value = "新增邀请码")
    @PostMapping("/add")
    public BaseResponse<String> add(@RequestBody AppInviteAddDto param){

        try {
            if(param == null || GlobalUtils.checkIsNull(param.getCount())){
                throw new RuntimeException("参数错误");
            }
            if(param.getCount() <= 0){
                throw new RuntimeException("数量不能小于1");
            }

            int count = appInviteService.genNewInviteCodes(param.getCount());

            return BaseResponse.success("成功添加" + count + "条");
        }catch (Exception e){
            e.printStackTrace();
            return BaseResponse.error(e.getMessage());
        }
    }

    @ApiOperation(value = "分页查询")
    @GetMapping("/page")
    public BaseResponse<IPage<AppInvite>> page(AppInviteRequestDto param){

        try {
            ParamUtils.checkParam(param);
            IPage<AppInvite> page = appInviteService.queryPage(param);

            return BaseResponse.success(page);
        }catch (Exception e){
            e.printStackTrace();
            return BaseResponse.error(e.getMessage());
        }
    }

}

