package com.village.friend.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.village.friend.dto.request.AppUserProfitQueryDto;
import com.village.friend.dto.response.BaseResponse;
import com.village.friend.entity.AppUserProfit;
import com.village.friend.service.AppUserProfitService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 用户收益明细表 前端控制器
 * </p>
 *
 * @author yl
 * @since 2021-08-23
 */
@RestController
@RequestMapping("/appUserProfit")
@Api(tags = "用户收益管理")
public class AppUserProfitController {


    @Autowired
    private AppUserProfitService service;

    @ApiOperation(value = "查询分润信息")
    @GetMapping("/list")
    public BaseResponse<IPage<AppUserProfit>> add(AppUserProfitQueryDto param){

        if(param == null){
            return BaseResponse.paramsError();
        }
        try {
            return BaseResponse.success(service.pageByParam(param));
        }catch (Exception e){
            e.printStackTrace();
            return BaseResponse.error(e.getMessage());
        }
    }

}

