package com.village.friend.controller;


import com.village.friend.dto.request.PolicyAddDto;
import com.village.friend.dto.request.PolicyInitDto;
import com.village.friend.dto.request.PolicyListDto;
import com.village.friend.dto.request.PolicyUpdateDto;
import com.village.friend.dto.response.BaseResponse;
import com.village.friend.entity.AppUserPolicy;
import com.village.friend.service.AppUserPolicyService;
import com.village.friend.utils.GlobalUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 会员缴费政策表 前端控制器
 * </p>
 *
 * @author yl
 * @since 2021-08-23
 */
@RestController
@RequestMapping("/appUserPolicy")
@Api(tags = "会员缴费政策管理")
public class AppUserPolicyController {

    @Autowired
    private AppUserPolicyService policyService;

    @ApiOperation("策略查询")
    @GetMapping("/list")
    public BaseResponse<List<AppUserPolicy>> list(PolicyListDto param) {
        if (param == null || GlobalUtils.checkIsNull(param.getPolicyType())) {
            return BaseResponse.paramsError();
        }
        try {
            List<AppUserPolicy> policies = policyService.queryParam(param);
            return BaseResponse.success(policies);
        }catch (Exception e){
            e.printStackTrace();
            return BaseResponse.error(e.getMessage());
        }
    }

    @ApiOperation("初始化升级配置")
    @PostMapping("/initPolicy")
    public BaseResponse<String> initPolicy(@RequestBody @Validated PolicyInitDto param) {
        if (param == null || GlobalUtils.checkIsNull(param.getMonth1(),param.getMonth3(), param.getMonth12())) {
            return BaseResponse.paramsError();
        }
        try {
            policyService.initPolicy(param);
            return BaseResponse.success();
        }catch (Exception e){
            e.printStackTrace();
            return BaseResponse.error(e.getMessage());
        }
    }

    @ApiOperation("添加升级配置")
    @PostMapping("/addPolicy")
    public BaseResponse<String> addPolicy(@RequestBody @Validated PolicyAddDto param) {
        if (param == null
                || GlobalUtils.checkIsNull(param.getAmount(),param.getTime())) {
            return BaseResponse.paramsError();
        }
        try {
            policyService.addPolicy(param);
            return BaseResponse.success();
        }catch (Exception e){
            e.printStackTrace();
            return BaseResponse.error(e.getMessage());
        }
    }

    @ApiOperation("修改升级配置")
    @PostMapping("/updatePolicy")
    public BaseResponse<String> updatePolicy(@RequestBody @Validated PolicyUpdateDto param) {
        if (param == null
                || GlobalUtils.checkIsNull(param.getPolicyContent(),
                param.getPolicyGrade(), param.getPolicyType(), param.getSourceGrade())) {
            return BaseResponse.paramsError();
        }
        try {
            policyService.updatePolicy(param);
            return BaseResponse.success();
        }catch (Exception e){
            e.printStackTrace();
            return BaseResponse.error(e.getMessage());
        }
    }
}

