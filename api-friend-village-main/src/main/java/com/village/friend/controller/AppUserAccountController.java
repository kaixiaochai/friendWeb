package com.village.friend.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.village.friend.dto.request.AccountDto;
import com.village.friend.dto.response.BaseResponse;
import com.village.friend.entity.AppUserAccount;
import com.village.friend.service.AppUserAccountService;
import com.village.friend.utils.ParamUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 用户账户表  前端控制器
 * </p>
 *
 * @author yl
 * @since 2021-08-21
 */
@RestController
@RequestMapping("/appUserAccount")
@Api(tags = "用户账户管理")
public class AppUserAccountController {

    @Autowired
    AppUserAccountService service;

    @ApiOperation(value = "分页查询")
    @GetMapping("/page")
    public BaseResponse<IPage<AppUserAccount>> page(AccountDto param){

        try {
            ParamUtils.checkParam(param);
            IPage<AppUserAccount> page = service.queryPage(param);

            return BaseResponse.success(page);
        }catch (Exception e){
            e.printStackTrace();
            return BaseResponse.error(e.getMessage());
        }
    }

}

