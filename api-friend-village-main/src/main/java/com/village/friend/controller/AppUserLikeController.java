package com.village.friend.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.village.friend.constant.MsgCodeEnum;
import com.village.friend.dto.request.UserLikeAddDto;
import com.village.friend.dto.request.UserLikeListDto;
import com.village.friend.dto.response.BaseResponse;
import com.village.friend.entity.User;
import com.village.friend.service.AppUserLikeService;
import com.village.friend.service.impl.UserServiceImpl;
import com.village.friend.utils.GlobalUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 用户关注表 前端控制器
 * </p>
 *
 * @author yl
 * @since 2021-09-01
 */
@RestController
@RequestMapping("/appUserLike")
@Api(tags = "用户关注模块")
public class AppUserLikeController {


    @Autowired
    private UserServiceImpl userService;

    @Autowired
    private AppUserLikeService service;

    @ApiOperation(value = "添加或删除喜欢")
    @PostMapping(value = "/like")
    public BaseResponse<String> like(@RequestBody UserLikeAddDto param) {
        if (param == null || GlobalUtils.checkIsNull(param.getLike(), param.getLikeUser(), param.getUsername())) {
            return BaseResponse.paramsError();
        }
        if(param.getLike() != 0 && param.getLike() != 1){
            return BaseResponse.paramsError("like参数不对!!");
        }
        User user = userService.findByUsername(param.getUsername());
        if(user == null){
            return BaseResponse.error("用户不存在");
        }
        User likeuser = userService.findByUsername(param.getLikeUser());
        if(likeuser == null){
            return BaseResponse.error("喜欢的用户不存在");
        }

        try {
            service.like(param);
            return BaseResponse.success();
        } catch (Exception e) {
            e.printStackTrace();
            return BaseResponse.error(e.getMessage());
        }
    }


    @ApiOperation(value = "分页查询")
    @GetMapping("/page")
    public BaseResponse<IPage<User>> userPage(UserLikeListDto param) {
        if (param == null) {
            return BaseResponse.paramsError();
        }
        try {
            IPage<User> data = service.pageByParam(param);
            return BaseResponse.success(data);
        }catch (Exception e){
            e.printStackTrace();
            return BaseResponse.error(e.getMessage());
        }
    }

}

