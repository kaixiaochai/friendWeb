package com.village.friend.controller;


import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author yl
 * @since 2021-08-17
 */
@RestController
@RequestMapping("/appUserMapping")
@Api(tags = "用户关系管理")
public class AppUserMappingController {

}

