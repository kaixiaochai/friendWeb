CREATE TABLE `app_payment_order` (
   `id` varchar(32) NOT NULL COMMENT 'ID主键',
   `user_id` varchar(32) DEFAULT NULL COMMENT '用户id',
   `full_name` varchar(128) DEFAULT NULL COMMENT '用户名称',
   `product_name` varchar(128) DEFAULT NULL COMMENT '商品名称',
   `order_amount` decimal(20,6) DEFAULT NULL COMMENT '订单金额',
   `profit_amount` decimal(20,6) DEFAULT NULL COMMENT '分润金额',
   `pay_status` varchar(64) DEFAULT NULL COMMENT '支付状态 见支付状态枚举',
   `failed_reason` varchar(512) DEFAULT NULL COMMENT '失败原因',
   `order_period` smallint(6) DEFAULT NULL COMMENT '订单有效期',
   `expire_time` datetime DEFAULT NULL COMMENT '到期时间',
   `remark` varchar(512) DEFAULT NULL COMMENT '备注',
   `pay_way_name` varchar(64) DEFAULT NULL COMMENT '支付渠道名称',
   `pay_type_no` varchar(32) DEFAULT NULL COMMENT '支付类型编码',
   `pay_type_name` varchar(64) DEFAULT NULL COMMENT '支付类型名称',
   `split_flag` varchar(1) DEFAULT NULL COMMENT '是否分账  0：否 1：是 ',
   `recon_flag` varchar(1) DEFAULT NULL COMMENT '是否对账  0：否 1：是 ',
   `create_time` timestamp NOT NULL DEFAULT current_timestamp() COMMENT '创建时间',
   `update_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT '修改时间',
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='交易订单';

CREATE TABLE `app_user_account` (
  `id` varchar(32) NOT NULL COMMENT 'id',
  `user_id` varchar(32) DEFAULT NULL COMMENT '用户id',
  `total_profit` decimal(9,2) DEFAULT NULL COMMENT '总收益',
  `balance` decimal(9,2) DEFAULT NULL COMMENT '账户余额',
  `create_time` timestamp NOT NULL DEFAULT current_timestamp() COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户账户表 ';

CREATE TABLE `app_user_profit` (
   `id` varchar(32) NOT NULL COMMENT 'id',
   `user_id` varchar(32) DEFAULT NULL COMMENT '用户ID（收益人user_id）',
   `full_name` varchar(32) DEFAULT NULL COMMENT '用户名字（收益人名字）',
   `trans_user_id` varchar(32) DEFAULT NULL COMMENT '交易用户ID（触发人user_id）',
   `trans_full_name` varchar(32) DEFAULT NULL COMMENT '交易用户名字',
   `trans_amount` decimal(9,2) DEFAULT NULL COMMENT '交易金额',
   `order_no` varchar(40) DEFAULT NULL COMMENT '交易订单号',
   `user_profit` decimal(9,2) DEFAULT NULL COMMENT '用户收益',
   `profit_percent` decimal(4,2) DEFAULT NULL COMMENT '分润比例',
   `profit_type` int(11) DEFAULT 0 COMMENT '分润类型',
   `remark` varchar(32) DEFAULT NULL COMMENT '备注',
   `create_time` timestamp NULL DEFAULT current_timestamp(),
   `update_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT '更新时间',
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户收益明细表';


CREATE TABLE `app_user_policy` (
   `id` int NOT NULL AUTO_INCREMENT COMMENT 'id',
   `policy_type` varchar(32) DEFAULT NULL COMMENT '政策类型 pay，customer，transamount',
   `type_desc` varchar(32) DEFAULT NULL COMMENT '类型描述 缴费，消费，交易金额，交易笔数，推荐',
   `policy_content` varchar(512) DEFAULT NULL COMMENT '政策内容',
   `policy_grade` varchar(32) DEFAULT NULL COMMENT '目标级别',
   `policy_name` varchar(32) DEFAULT NULL COMMENT '政策名称',
   `source_grade` varchar(32) DEFAULT NULL COMMENT '开始级别,升级条件',
   `create_time` timestamp NOT NULL DEFAULT current_timestamp() COMMENT '创建时间',
   `update_time` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT '更新时间',
   PRIMARY KEY (`id`),
   UNIQUE KEY `level_up_setting_index` (`policy_type`,`policy_grade`,`source_grade`) COMMENT '升级付费唯一'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='会员缴费政策表';

CREATE TABLE `app_profit_policy` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'id',
  `col_group` varchar(32) NOT NULL COMMENT '功能分组',
  `col_key` varchar(32) NOT NULL COMMENT '功能标识',
  `col_val` varchar(256) DEFAULT NULL COMMENT '功能值',
  `remark` varchar(32) DEFAULT NULL COMMENT '备注',
   PRIMARY KEY (`id`),
  UNIQUE KEY `group_key` (`col_group`,`col_key`) COMMENT '功能组的唯一键'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='分润政策表';


CREATE TABLE `t_trade_product` (
     `id` varchar(32) NOT NULL,
     `product_no` varchar(32) DEFAULT NULL COMMENT '产品编码，来自外界编码，兼容渠道编码',
     `product_name` varchar(64) DEFAULT NULL COMMENT '产品名称',
     `channel_no` varchar(32) DEFAULT NULL COMMENT '关联渠道表编码',
     `single_limit` int(11) DEFAULT 100000 COMMENT '单笔限额',
     `day_limit` int(11) DEFAULT 100000 COMMENT '日限额',
     `remark` varchar(512) DEFAULT NULL COMMENT '备注',
     `create_time` timestamp NOT NULL DEFAULT current_timestamp() COMMENT '创建时间',
     `update_time` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT '更新时间',
     PRIMARY KEY (`id`),
     KEY `channel_product_index` (`product_no`,`channel_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='渠道产品表';
