package com.village.friend.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.village.friend.entity.AppUserQuestion;

/**
 * <p>
 * 用户反馈表 Mapper 接口
 * </p>
 *
 * @author yl
 * @since 2021-09-23
 */
public interface AppUserQuestionMapper extends BaseMapper<AppUserQuestion> {

}
