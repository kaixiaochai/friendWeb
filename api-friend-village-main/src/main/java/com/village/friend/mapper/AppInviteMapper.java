package com.village.friend.mapper;

import com.village.friend.entity.AppInvite;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yl
 * @since 2021-08-17
 */
public interface AppInviteMapper extends BaseMapper<AppInvite> {

    AppInvite findOneDotUseInviteCode();

}
