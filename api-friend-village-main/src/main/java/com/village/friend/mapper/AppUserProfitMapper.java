package com.village.friend.mapper;

import com.village.friend.entity.AppUserProfit;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户收益明细表 Mapper 接口
 * </p>
 *
 * @author yl
 * @since 2021-08-23
 */
public interface AppUserProfitMapper extends BaseMapper<AppUserProfit> {

}
