package com.village.friend.mapper;

import com.village.friend.entity.AppPaymentOrder;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 交易订单 Mapper 接口
 * </p>
 *
 * @author yl
 * @since 2021-08-23
 */
public interface AppPaymentOrderMapper extends BaseMapper<AppPaymentOrder> {

}
