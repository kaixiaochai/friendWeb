package com.village.friend.mapper;

import com.village.friend.entity.AppUserAccount;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户账户表  Mapper 接口
 * </p>
 *
 * @author yl
 * @since 2021-08-21
 */
public interface AppUserAccountMapper extends BaseMapper<AppUserAccount> {

}
