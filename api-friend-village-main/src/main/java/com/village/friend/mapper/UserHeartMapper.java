package com.village.friend.mapper;

import com.village.friend.entity.UserHeart;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Insert;

/**
 * <p>
 * 用户心跳 Mapper 接口
 * </p>
 *
 * @author yl
 * @since 2021-08-13
 */
public interface UserHeartMapper extends BaseMapper<UserHeart> {

    @Insert("insert into  user_heart(username,ip,latitude,longitude)  values (#{username},#{ip},#{latitude},#{longitude}) on  duplicate key update  ip=#{ip},latitude=#{latitude},longitude=#{longitude}")
    Integer add(String username, String ip, Double latitude, Double longitude);

}
