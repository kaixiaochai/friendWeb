package com.village.friend.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.village.friend.entity.User;
import com.village.friend.entity.UserHeart;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Collection;
import java.util.List;

public interface UserMapper extends BaseMapper<User> {


    User findParentByLevel(@Param("username") String username, @Param("level") Integer level);
}
