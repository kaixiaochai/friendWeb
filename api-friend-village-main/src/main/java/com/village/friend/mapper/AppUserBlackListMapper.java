package com.village.friend.mapper;

import com.village.friend.entity.AppUserBlackList;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户黑名单表 Mapper 接口
 * </p>
 *
 * @author yl
 * @since 2021-09-02
 */
public interface AppUserBlackListMapper extends BaseMapper<AppUserBlackList> {

}
