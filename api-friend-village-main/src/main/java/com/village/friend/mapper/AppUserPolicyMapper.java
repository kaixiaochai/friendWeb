package com.village.friend.mapper;

import com.village.friend.entity.AppUserPolicy;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 会员缴费政策表 Mapper 接口
 * </p>
 *
 * @author yl
 * @since 2021-08-23
 */
public interface AppUserPolicyMapper extends BaseMapper<AppUserPolicy> {

}
