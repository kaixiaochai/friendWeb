package com.village.friend.mapper;

import com.village.friend.entity.UserAuth;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface UserAuthMapper {
    @Insert("insert into  user_auth(username,auth_code) values (#{username},#{authCode}) on  duplicate key update  auth_code=#{authCode}")
    Integer add(String username, String authCode);

    @Insert("insert into  user_auth(username,auth_code,token) values (#{username},#{authCode},#{token}) on  duplicate key update  auth_code=#{authCode} ,token=#{token}")
    Integer update(String username, String authCode, String token);

    @Select("select username,password,auth_code authCode,token  from user_auth where username = #{username}")
    UserAuth get(String username);

    @Select("select username,password,auth_code authCode,token  from user_auth where token = #{token}")
    UserAuth findByToken(String token);


}
