package com.village.friend.mapper;

import com.village.friend.entity.AppProfitPolicy;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 分润政策表 Mapper 接口
 * </p>
 *
 * @author yl
 * @since 2021-08-23
 */
public interface AppProfitPolicyMapper extends BaseMapper<AppProfitPolicy> {

}
