package com.village.friend.mapper;

import com.village.friend.entity.Activity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yl
 * @since 2021-08-13
 */
public interface ActivityMapper extends BaseMapper<Activity> {


}
