package com.village.friend.mapper;

import com.village.friend.entity.AppUserLike;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.village.friend.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 用户关注表 Mapper 接口
 * </p>
 *
 * @author yl
 * @since 2021-09-01
 */
public interface AppUserLikeMapper extends BaseMapper<AppUserLike> {


}
