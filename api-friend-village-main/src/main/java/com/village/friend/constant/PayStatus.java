package com.village.friend.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

/**
 * 订单状态
 */
@Getter
@ToString
public enum PayStatus {
    FAILED("FAILED"),SUCCESS("SUCCESS"),IN_PROCESS("IN_PROCESS");
    private String value;
    PayStatus(String value){
        this.value = value;
    }



    public static PayStatus valueTrans(String status){
        if(status.equals(SUCCESS.getValue()))return SUCCESS;
        else if(status.equals(FAILED.getValue())) return FAILED;
        else return IN_PROCESS;
    }
}
