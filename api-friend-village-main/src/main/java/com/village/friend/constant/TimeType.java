package com.village.friend.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

/**
 * 选择时间的 时间类型
 */
@Getter
@AllArgsConstructor
@ToString
public enum TimeType {

    ANYTIME(1, "随时"),
    DISTIME(2, "可商议"),
    SPECTIME(3, "具体时间");
    private int code;
    private String msg;
}
