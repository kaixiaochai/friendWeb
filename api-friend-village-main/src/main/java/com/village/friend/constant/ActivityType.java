package com.village.friend.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

/**
 * 活动类型
 */
@Getter
@AllArgsConstructor
@ToString
public enum ActivityType {

    LX(1, "旅行"),
    JH(2, "聚会"),
    YF(3, "约饭"),
    KDY(4, "看电影"),
    BD(5, "蹦迪");
    private int code;
    private String msg;
}
