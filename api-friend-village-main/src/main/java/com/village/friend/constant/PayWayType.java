package com.village.friend.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

/**
 * 活动类型
 */
@Getter
@AllArgsConstructor
@ToString
public enum PayWayType {

    WECHAT("wechat", "微信"),
    ALIPAYAPP("aliAppPay", "支付宝app"),
    ALIPAYH5("aliH5Pay", "支付宝h5");
    private String type;
    private String name;
}
