package com.village.friend.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

/**
 * 期待对象的类型
 */
@Getter
@AllArgsConstructor
@ToString
public enum DestType {

    GFS(1, "高富帅"),
    BFM(2, "白富美"),
    MMZ(3, "萌妹子"),
    BDZC(4, "霸道总裁"),
    WYQN(5, "文艺青年"),
    DS(6, "大叔"),
    XXR(5, "小鲜肉");
    private int code;
    private String msg;
}
