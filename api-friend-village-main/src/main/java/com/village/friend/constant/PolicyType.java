package com.village.friend.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

/**
 * 缴费策略类型
 */
@Getter
@AllArgsConstructor
@ToString
public enum PolicyType {

    UPGRADE("UPGRADE", "升级缴费"),
    UNKNOW("UNKNOW", "未知类型");
    private String type;
    private String name;

    public static PolicyType valueTrans(String type){
        if(type.equals(UPGRADE.getType()))return UPGRADE;
        else return UNKNOW;
    }
}
