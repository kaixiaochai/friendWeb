package com.village.friend.constant;

public interface AttributeCallBack<T>{

    String callBack(T t);
}
