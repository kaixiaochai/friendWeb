package com.village.friend.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * Author     : santana
 * DateTime   : 2017/7/13 11:45
 * Description: 配置默认自动填充 http://mp.baomidou.com/#/auto-fill
 */
@Component
public class MyBatisFillHandler implements MetaObjectHandler {

    private static String createTimeFieldName = "createTime";
    private static String updateTimeFieldName = "updateTime";

    //新增填充
    @Override
    public void insertFill(MetaObject metaObject) {
        setFieldValue(createTimeFieldName, LocalDateTime.now(), metaObject);
        setFieldValue(updateTimeFieldName, LocalDateTime.now(), metaObject);
    }

    //更新填充
    @Override
    public void updateFill(MetaObject metaObject) {
        setFieldValue(updateTimeFieldName, LocalDateTime.now(), metaObject);
    }

    private void setFieldValue(String fileName, LocalDateTime currentDate, MetaObject metaObject) {
        Object fileValue = getFieldValByName(fileName, metaObject);
        if (null == fileValue) {
            setFieldValByName(fileName, currentDate, metaObject);
        }
    }

}
