package com.village.friend.config;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author: santana
 * @date: 2017/11/8 11:39
 * @description:
 */

@Configuration
public class MybatisPlusConfig {

    /**
     * 分页插件
     * 开启 PageHelper 的支持
     *
     * @return
     */
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        PaginationInterceptor page = new PaginationInterceptor();
        page.setDbType(DbType.MYSQL);
        return page;
    }

}
