package com.village.friend.utils;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.village.friend.constant.AttributeCallBack;
import com.village.friend.dto.response.BaseResponse;
import com.village.friend.entity.User;
import com.village.friend.service.impl.UserServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

public class GlobalUtils {
    public static boolean checkIsNull(Object ... params){
        return !checkIsNotNull(params);
    }

    /**
     * 检查空
     * @param params
     * @return 不为null 或者 "" 返回true 否则返回 false
     */
    public static boolean checkIsNotNull(Object ... params){
        if(params == null || params.length == 0)return true;
        for (int i = 0; i < params.length; i++) {
            if(params[i] == null)return false;
            if(params[i] instanceof CharSequence){
                if(StringUtils.isEmpty((CharSequence) params[i]))return false;
            }
        }
        return true;
    }


    public static String generateNo(int index){
        if(index > 99){
            return "" + index;
        }else if(index > 9){
            return "0" + index;
        }
        return "00" + index;
    }

    public static <T> List<T> parseList(String json, Class<T> tClass){
        List<T> list = new ArrayList<>();

        if(json == null){
            return list;
        }
        try {
            list = JSON.parseArray(json,tClass);
        }catch (Exception e){
            e.printStackTrace();
        }
        return list;
    }

    /**
     * 格式化数字
     * @param value  数据
     * @param scale  保留几位小数
     * @return
     */
    public static String formatNumber(double value,int scale) {

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(scale, RoundingMode.HALF_UP);
        return bd.toString();
    }

    /**
     * 除法
     */
    public static BigDecimal divide(String arg1, String arg2) {
        if (StringUtils.isEmpty(arg1)) {
            arg1 = "0.0";
        }
        if (StringUtils.isEmpty(arg2)) {
            arg2 = "0.0";
        }
        BigDecimal big3 = new BigDecimal("0.00");
        if (Double.parseDouble(arg2) != 0) {
            BigDecimal big1 = new BigDecimal(arg1);
            BigDecimal big2 = new BigDecimal(arg2);
            big3 = big1.divide(big2, 2, BigDecimal.ROUND_HALF_EVEN);
        }
        return big3;
    }

    public static boolean checkUser(UserServiceImpl userService, String username){
        User user = userService.findByUsername(username);
        return user != null;
    }

    /**
     * list转map
     * @param list 需要转换成map的list
     * @param callBack 用于选择key的接口
     * @param <T>
     * @return
     */
    public static <T> Map<String,T> transMap(List<T> list, AttributeCallBack<T> callBack){
        Map<String,T> map = new HashMap<>();
        if(list != null && list.size() > 0){
            for (int i = 0; i < list.size(); i++) {
                map.put(callBack.callBack(list.get(i)), list.get(i));
            }
        }

        return map;
    }


//    public static <T extends BaseEntity,Y extends BaseEntity>  void transAttribute(List<T> list, Map<String,Y> likeMap, AttributeCallBack<T> callBack){
//        //替换时间
//        if(list != null && list.size() > 0){
//            for (int i = 0; i < list.size(); i++) {
//                T user = list.get(i);
//                Y like = likeMap.get(user.getUsername());
//                user.setCreateTime(like.getCreateTime());
//            }
//        }
//    }


    /**
     * 把list每个item的特定属性生成一个set
     * @param list 需要被操作的list
     * @param callBack 选择属性的接口
     * @param <T>
     * @return
     */
    public static <T> Collection<String> transAttribute(List<T> list, AttributeCallBack<T> callBack){
        Collection<String> set = new TreeSet<>();
        if(list != null && list.size() > 0){
            for (T t : list) {
                set.add(callBack.callBack(t));
            }
        }
        return set;
    }


    /**
     * 把一个T泛型的list转换成一个Y泛型的list
     * @param list  T泛型list
     * @param yClass Y类型字节码
     * @param <T>
     * @param <Y>
     * @return
     * @throws IllegalAccessException
     * @throws InstantiationException
     */
    public static <T,Y>  List<Y> transList(List<T> list, Class<Y> yClass ) throws IllegalAccessException, InstantiationException {
        List<Y> ls = new ArrayList<>();
        if(list != null && list.size() > 0){
            for (T t : list) {
                Y y = yClass.newInstance();
                BeanUtils.copyProperties(t,y,yClass);
                ls.add(y);
            }
        }
        return ls;
    }


    /**
     * 把一个T泛型的IPage转换成一个Y泛型的IPage
     * @param orgPage
     * @param list
     * @param <T>
     * @param <Y>
     * @return
     */
    public static <T,Y> IPage<Y> transPage(IPage<T> orgPage, List<Y> list){
        IPage<Y> tPage = new Page<>();
        tPage.setTotal(orgPage.getTotal());
        tPage.setPages(orgPage.getPages());
        tPage.setCurrent(orgPage.getCurrent());
        tPage.setSize(orgPage.getSize());
        tPage.setRecords(list);
        return tPage;
    }
}
