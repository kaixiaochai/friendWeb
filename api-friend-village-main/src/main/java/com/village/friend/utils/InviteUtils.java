package com.village.friend.utils;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;

import java.util.*;

/**
 * 邀请码相关工具类
 */
public class InviteUtils {

    public static String getInviteCode() {
        Random rand = new Random();
        char[] letters = new char[]{'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q',
                'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i',
                'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'r',
                '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
        String str = "";
        int index;
        boolean[] flags = new boolean[letters.length];//默认为false
        for (int i = 0; i < 8; i++) {
            do {
                index = rand.nextInt(letters.length);
            } while (flags[index] == true);
            char c = letters[index];
            str += c;
            flags[index] = true;
        }
        System.out.println(str);
        return str;
    }

    public static Map<String, Object> reqParamTransForm(JSONArray jsonArray) {
        Map<String, Object> reqParam = new HashMap<>();
        if (CollectionUtils.isNotEmpty(jsonArray)) {
            for (int i = 0; i < jsonArray.size(); i++) {
                LinkedHashMap<String,Object> data = (LinkedHashMap)jsonArray.get(i);
                String key = (String)data.get("name");
                Object value = data.get("value");
                reqParam.put(key,value);
            }
        }
        return reqParam;
    }

    public static String genProductId(){
        return "PRO"+System.currentTimeMillis();
    }


    /**
     * 获取去掉横线的长度为32的UUID串.
     *
     * @author WuShuicheng.
     * @return uuid.
     */
    public static String get32UUID() {
        return UUID.randomUUID().toString().replace("-", "");
    }

    /**
     * 获取带横线的长度为36的UUID串.
     *
     * @author WuShuicheng.
     * @return uuid.
     */
    public static String get36UUID() {
        return UUID.randomUUID().toString();
    }


}
